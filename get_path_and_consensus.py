from os import listdir
from sys import argv
import numpy as np
from OptiAsm import pathfinder
from OptiAsm.consensus import Consensus, optimize_consensus
from OptiScan import database as db

folder = argv[1]
mols = np.load(argv[2])

corrs = []
files = [folder + x for x in listdir(folder)]
_ = [[corrs.append(y) for y in np.load(f)] for f in files]
corrs = [(x,(y,z)) for x,y,z in corrs]
qual = pathfinder.MQR("./mqr_res/", "/mnt/scratch/akdel/tools/RefAligner")
paths = [x for x in qual.corrs_to_paths2(corrs, bet_thr=0.018, edge_bet=0.015) if len(x) > 2]

consensus = []
for path in paths:
    c = Consensus(corrs)
    c.from_path_to_space2(path[0],path[1]),path
    path_mols = path[0]
    c.fill_contained_space(path_mols,mols, containing=True)
    c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=True)
    c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=True)
    c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=False)
    consensus.append(c)

n = 0
for c in consensus:
    n += 1
    min([x.begin for x in c.space.all_intervals]), max([x.end for x in c.space.all_intervals])
    arr = np.zeros((len(c.space.all_intervals), max([x.end for x in c.space.all_intervals]) + 100), dtype=float)
    arr[:] = np.NaN
    i = 0
    for start, stop, mol_id, flip in sorted(
            [(x.begin, x.end, x.data[0], x.data[1]) for x in c.space.all_intervals if x.data != "consensus"]):
        if flip == False:
            m = mols[mol_id][::-1]
        else:
            m = mols[mol_id]
        try:
            arr[i, start:start + m.shape[0]] = m
            i += 1
        except:
            continue
    optimize_consensus(arr, 20)
    quart = np.nan_to_num(np.nanpercentile(arr, 50, axis=0))
    db.molecules_to_bnx([(x, x) for x in quart], zoom_ratio=1, final_ratio=510,
                        bnx_filename="contig%s.bnx" % n, signal_to_noise_ratio=2.5,
                        bnx_template_path="bnx_head.txt")