import numpy as np
from OptiAsm import pairwise_cross_corr as pw
import multiprocessing as mp
from sys import argv
from time import sleep, time

if __name__ == "__main__":
    mol_file = argv[1]
    thread_num = int(argv[2])
    max_threads = int(argv[3])
    mol_group_start = int(argv[4])
    mol_group_end = int(argv[5])
    mols = pw.to_log_mols(np.load(mol_file))
    print(len(mols))
    log_file = open("log%s-%s.txt" % (mol_group_start, mol_group_end), "w")
    log_file.write("log_mols\n")
    fft_mols = pw.CrossCorrInput(mols, thread_num, trunct=1024, to_log=True, spectra_ratio=0.6)
    fft_mols.split_to_thread_jobs_in_sequence(proportionate=False)
    m = pw.CrossCorrMemmap(fft_mols, "/home/akdel001/cross_corr.mem")
    if mol_group_start == 0:
        m._initiate_memmap()
    else:
        pass
    for i in range(mol_group_start, mol_group_end, max_threads):
        jobs = [mp.Process(target=m.correlate_chunk_and_insert_to_memmap, args=(x,)) for x in fft_mols.molecule_groups[i:i+max_threads]]
        _ = [x.start() for x in jobs]
        t = time()
        while sum([x.is_alive() for x in jobs]) != 0:
            sleep(1)
        _ = [x.terminate() for x in jobs]
        log_file.write("%s\t%s\t%s\n" % (i, i+max_threads, time() - t))
        log_file.flush()
        del jobs
    log_file.close()