import networkx as nx
from OptiMap import correlation_struct as cs
from OptiMap import molecule_struct as ms
from OptiAsm import *
# import graph_tool as gt
# from graph_tool import centrality, topology, search

SNR = 4
DTYPE = np.dtype({'names': ["overlap_score", "long_id", "short_id", "long_start", "long_end",
                            "short_start", "short_end", "long_len", "short_len", "contained", "reversed"],
                  'formats': [np.float64, np.int64, np.int64, np.float64, np.float64, np.float64,
                                np.float64, np.float64, np.float64, np.bool, np.bool]})

class OptiGraph:
    def __init__(self, edges, number_of_nodes):
        self.nnodes = number_of_nodes
        self.edges = edges
        self.directed = gt.Graph(directed=True)
        self.directed_non_string = gt.Graph(directed=True)
        self._load_vertices()
        self._load_edges(directed=True)
        self._load_edges(directed=False)
        self.component_graphs = list()
        self.component_freq = None
        self.filter_graph = False
    
    def _load_vertices(self):
        self.directed.add_vertex(self.nnodes * 2)
        self.directed_non_string.add_vertex(self.nnodes)
        self._load_properties()
        for vertex in self.directed.vertices():
            self.directed.vertex_properties["id"][vertex] = int(vertex)
        for vertex in self.directed_non_string.vertices():
            self.directed_non_string.vertex_properties["id"][vertex] = int(vertex)

    def _load_properties(self):
        self.directed.edge_properties["overlap"] = self.directed.new_edge_property("vector<double>")
        self.directed.edge_properties["type"] = self.directed.new_edge_property("vector<bool>")
        self.directed.edge_properties["short_id"] = self.directed.new_edge_property("int32_t")
        self.directed.edge_properties["long_id"] = self.directed.new_edge_property("int32_t")
        self.directed.edge_properties["short_ori"] = self.directed.new_edge_property("bool")
        self.directed.edge_properties["short_len"] = self.directed.new_edge_property("double")
        self.directed.edge_properties["long_len"] = self.directed.new_edge_property("double")
        self.directed.edge_properties["score"] = self.directed.new_edge_property("double")
        self.directed.edge_properties["betweeness"] = self.directed.new_edge_property("double")
        self.directed.edge_properties["overlap_len"] = self.directed.new_edge_property("double")
        self.directed.edge_properties["overlap_len_neg"] = self.directed.new_edge_property("double")
        self.directed.vertex_properties["betweeness"] = self.directed.new_vertex_property("double")
        self.directed.vertex_properties["component"] = self.directed.new_vertex_property("int32_t")
        self.directed.vertex_properties["id"] = self.directed.new_vertex_property("int32_t")

        self.directed_non_string.edge_properties["overlap"] = self.directed_non_string.new_edge_property("vector<double>")
        self.directed_non_string.edge_properties["type"] = self.directed_non_string.new_edge_property("vector<bool>")
        self.directed_non_string.edge_properties["short_id"] = self.directed_non_string.new_edge_property("int32_t")
        self.directed_non_string.edge_properties["long_id"] = self.directed_non_string.new_edge_property("int32_t")
        self.directed_non_string.edge_properties["short_ori"] = self.directed_non_string.new_edge_property("bool")
        self.directed_non_string.edge_properties["short_len"] = self.directed_non_string.new_edge_property("double")
        self.directed_non_string.edge_properties["long_len"] = self.directed_non_string.new_edge_property("double")
        self.directed_non_string.edge_properties["score"] = self.directed_non_string.new_edge_property("double")
        self.directed_non_string.edge_properties["betweeness"] = self.directed_non_string.new_edge_property("double")
        self.directed_non_string.edge_properties["overlap_len"] = self.directed_non_string.new_edge_property("double")
        self.directed_non_string.edge_properties["overlap_len_neg"] = self.directed_non_string.new_edge_property("double")
        self.directed_non_string.vertex_properties["betweeness"] = self.directed_non_string.new_vertex_property("double")
        self.directed_non_string.vertex_properties["component"] = self.directed_non_string.new_vertex_property("int32_t")
        self.directed_non_string.vertex_properties["id"] = self.directed_non_string.new_vertex_property("int32_t")



    def _load_edges(self, directed=True):
        shift = self.nnodes
        if directed:
            g = self.directed
        else:
            g = self.directed_non_string
        prop = g.edge_properties
        not_contained_edges = filter(lambda x: not x["contained"], self.edges)
        for edge in not_contained_edges:
            short_id, long_id = (edge["short_id"], edge["long_id"])
            if edge["long_start"] > 0:
                if edge["short_start"] < edge["short_end"]:
                    if directed:
                        g_edge_f = g.add_edge(long_id, short_id)
                        g_edge_r = g.add_edge(short_id + shift, long_id + shift)
                    else:
                        g_edge_f = g.add_edge(long_id, short_id)
                        g_edge_r = g.add_edge(short_id, long_id)
                    prop["overlap"][g_edge_f] = [edge["long_start"], edge["long_end"],edge["short_start"], edge["short_end"]]
                    prop["overlap"][g_edge_r] = [edge["short_start"], edge["short_end"],edge["long_start"], edge["long_end"]]
                    prop["type"][g_edge_f] = (True, True)
                    prop["type"][g_edge_r] = (False, False)
                else:
                    if directed:
                        g_edge_f = g.add_edge(long_id, short_id + shift)
                        g_edge_r = g.add_edge(short_id, long_id + shift)
                    else:
                        g_edge_f = g.add_edge(long_id, short_id)
                        g_edge_r = g.add_edge(short_id, long_id)
                    prop["overlap"][g_edge_f] = [edge["long_start"], edge["long_end"], edge["short_start"], edge["short_end"]]
                    prop["overlap"][g_edge_r] = [edge["short_start"], edge["short_end"], edge["long_start"], edge["long_end"]]
                    prop["type"][g_edge_f] = (True, False)
                    prop["type"][g_edge_r] = (True, False)
            else:
                if edge["short_start"] < edge["short_end"]:
                    if directed:
                        g_edge_f = g.add_edge(short_id, long_id)
                        g_edge_r = g.add_edge(long_id + shift, short_id + shift)
                    else:
                        g_edge_f = g.add_edge(short_id, long_id)
                        g_edge_r = g.add_edge(long_id, short_id)
                    prop["overlap"][g_edge_f] = [edge["short_start"], edge["short_end"], edge["long_start"], edge["long_end"]]
                    prop["overlap"][g_edge_r] = [edge["long_start"], edge["long_end"], edge["short_start"], edge["short_end"]]
                    prop["type"][g_edge_f] = (True, True)
                    prop["type"][g_edge_r] = (False, False)
                else:
                    if directed:
                        g_edge_f = g.add_edge(short_id + shift, long_id)
                        g_edge_r = g.add_edge(long_id + shift, short_id)
                    else:
                        g_edge_f = g.add_edge(short_id, long_id)
                        g_edge_r = g.add_edge(long_id, short_id)
                    prop["overlap"][g_edge_f] = [edge["short_start"], edge["short_end"], edge["long_start"], edge["long_end"]]
                    prop["overlap"][g_edge_r] = [edge["long_start"], edge["long_end"], edge["short_start"], edge["short_end"]]
                    prop["type"][g_edge_f] = (False, True)
                    prop["type"][g_edge_r] = (False, True)
            prop["short_id"][g_edge_f] = edge["short_id"]
            prop["short_id"][g_edge_r] = edge["short_id"]
            prop["long_id"][g_edge_f] = edge["long_id"]
            prop["long_id"][g_edge_r] = edge["long_id"]
            prop["score"][g_edge_f] = edge["overlap_score"]
            prop["score"][g_edge_r] = edge["overlap_score"]
            ov_len = abs(edge["long_start"] - edge["long_end"])
            prop["overlap_len"][g_edge_r] = ov_len
            prop["overlap_len"][g_edge_f] = ov_len
            prop["overlap_len_neg"][g_edge_r] = -ov_len
            prop["overlap_len_neg"][g_edge_f] = -ov_len

    def unidirectional_betweeness(self, edge_betweeness, vertex_betweeness, lim=500000, directed=True, norm=False):
        if directed:
            g = self.directed
            sample_from = np.arange(self.nnodes * 2)
            rand_sample = np.random.choice(sample_from, size=lim, replace=False)
        else:
            g = self.directed_non_string
            sample_from = np.arange(self.nnodes)
            rand_sample = np.random.choice(sample_from, size=lim, replace=False)
        g.set_directed(False)
        pv, pe = centrality.betweenness(g, pivots=rand_sample, norm=norm)
        g.set_directed(True)
        thre = np.sort(pe.a)[-1 * int(g.num_edges()*edge_betweeness)]
        thrv = np.sort(pv.a)[-1 * int(g.num_vertices()*vertex_betweeness)]
        filtered = gt.Graph(g=gt.GraphView(g, efilt=lambda x:pe[x] > thre), prune=True, directed=True)
        banned_edges = set([tuple(sorted((int(x.source()), int(x.target())))) for x in filtered.edges()])
        banned_vertices = set([int(x) for x in g.vertices() if pv[x] > thrv])
        return banned_edges, banned_vertices

    def filter_with_banned_vertices_and_edges(self, g, banned_edges, banned_vertices):
        efilt_func = lambda x: False if tuple(sorted((int(x.source()), int(x.target())))) in banned_edges else True
        vfilt_func = lambda x: False if int(x) in banned_vertices else True
        return gt.Graph(g=gt.GraphView(g, efilt=efilt_func, vfilt=vfilt_func), prune=True, directed=True)

    def label_components(self, directed=True):
        if directed:
            g = self.directed
        else:
            g = self.directed_non_string
        g.vertex_properties["component"], self.component_freq = topology.label_components(gt.GraphView(g, directed=False))
        
    def obtain_component_graphs(self, thr=50, directed=True, generate=False):
        if directed:
            g = self.directed
        else:
            g = self.directed_non_string
        comp_ids = np.where(self.component_freq > thr)[0]
        if generate:
            for _id in comp_ids:
                yield self.return_component_as_graph(g, _id)
        else:
            for _id in comp_ids:
                self.component_graphs.append(self.return_component_as_graph(g, _id))

    def return_component_as_graph(self, graph, component_id):
        print(component_id)
        return gt.Graph(g=gt.GraphView(graph, vfilt=lambda x:graph.vertex_properties["component"][x] == component_id), prune=True, directed=True)

    def filter_according_to_betweeness(self, graph, thr, type):
        if type == "e":
            return gt.Graph(g=gt.GraphView(graph, efilt=lambda x:graph.edge_properties["betweeness"][x] < thr), prune=True, directed=True)
        elif type == "v":
            return gt.Graph(g=gt.GraphView(graph, vfilt=lambda x:graph.vertex_properties["betweeness"][x] < thr), prune=True, directed=True)
    
    def search_two_furthest_vertices(self, comp, weight_key, recorded_components=True):
        current_max = 0
        current_res = None
        for v in comp.vertices():
            p, _ = search.astar_search(comp, source=v, weight=comp.edge_properties["overlap_len"])
            p.a[np.where(p.a == np.inf)[0]] = -1
            argmax = np.argmax(p.a)
            if p.a[argmax] > current_max and p.a[argmax] != np.inf:
                current_res = (v, argmax, p.a[argmax])
                current_max = p.a[argmax]
        v1, v2, distance = current_res[0], comp.vertex(current_res[1]), current_res[-1]
        if comp.num_edges() > 3000:
            return v1, v2, distance, comp
        else:
            print("starting bellman")
            _,res,_ = search.bellman_ford_search(comp, source=v1, weight=comp.edge_properties["overlap_len_neg"])
            res.a[np.where(res.a == np.inf)[0]] = 1
            res = res.a
            v2 = np.argmin(res)
            v2 = comp.vertex(v2)
            return v1, v2, np.min(res), comp
    
    def find_longest_path(self, comp, source, target, real_ids=True):
        _all = topology.all_paths(comp, source, target)
        paths = []
        n = 0
        for p in _all:
            if n > 150:
                break
            else:
                n += 1
                paths.append(p)
        path = max([(x.shape[0], x) for x in paths], key=lambda x:x[0])[1]
        if real_ids:
            vertices = [comp.vertex_properties["id"][comp.vertex(x)] for x in path]
            change = lambda x: (x - self.nnodes) if x > self.nnodes else x
            return list(map(change, vertices))
        else:
            return path

    def clean_circuits(self, comp_mut, clean_repeat=10):
        trials = 0
        clean = True
        while True:
            if trials > clean_repeat:
                clean = False
                break
            try:
                circs = topology.all_circuits(comp_mut)
                c = 0
                circuits = list()
                for circ in circs:
                    if c > 10:
                        break
                    else:
                        circuits += list(circ)
                        c += 1
                print(len(circuits))
                circuits = np.array(circuits)
            except ValueError:
                break
            if circuits.shape[0] == 0:
                break
            else:
                uniques, counts = np.unique(circuits, return_counts=True)
                top_argmax = np.argmax(counts)
                top_id = uniques[top_argmax]
                comp_mut.remove_vertex(comp_mut.vertex(top_id))
            trials += 1
        return comp_mut, clean

    def single_component_to_path(self, comp, eb, vb, subsample=50000, clean_repeat=25):
        if not self.filter_graph:
            print("Estimating betweenness scores...")
            banned_edges, banned_vertices = self.unidirectional_betweeness(eb, vb, lim=subsample, directed=True, norm=False)
            self.directed = self.filter_with_banned_vertices_and_edges(self.directed.copy(), banned_edges, banned_vertices)
            self.filter_graph = True
            print("Done.")
        comp, clean = self.clean_circuits(comp, clean_repeat=clean_repeat)
        if clean:
            v1, v2, _, comp = self.search_two_furthest_vertices(comp, "overlap_len_neg")
            return self.find_longest_path(comp, v1, v2, real_ids=True)
        else:
            return []


    def path_generator(self, eb=0.01, vb=0.01, thr=50, subsample=50000, vertex_lim=20000, clean_repeat=25):
        for comp in self.obtain_component_graphs(thr=thr, directed=True, generate=True):
            if comp.num_vertices() > vertex_lim:
                print(comp.num_vertices(), comp.num_edges())
                continue
            print(comp.num_vertices(), comp.num_edges())
            path = self.single_component_to_path(comp, eb, vb, subsample=subsample, clean_repeat=clean_repeat)
            print(path)
            yield path
    

class StringGraph:
    def __init__(self, molecule_corr_info_stream, dot_pics_path="./"):
        self.stream = molecule_corr_info_stream
        self.dot_pics_path = dot_pics_path
        self.edges_for_string_graph = molecule_corr_info_stream
        self.contained_edges = list()
        self.graph = nx.DiGraph()
        self.contigs = dict()
        self.graph2 = nx.DiGraph()
        self.reverse_table = dict()

    def _filter_overlaps_by_score_peaks(self):
        pass

    def create_graph(self):
        contained_nodes = set()
        #for edge in self.edges_for_string_graph:
        #    if edge["contained"]:
        #        self.contained_edges.append(edge)
        #        contained_nodes.add(edge["short_id"])
        for edge in self.edges_for_string_graph:
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes:
                info_dict = dict()
                _long = edge["long_id"]
                _short = edge["short_id"]
                if edge["contained"] == False:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s .B" % _short, "%s .B" % _long, overhang=abs(edge["long_start"] - 0),
                                                label=[True, _long, edge["long_start"], 0])
                            self.graph.add_edge("%s .E" % _long, "%s .E" % _short, overhang=abs(edge["short_end"] -
                                                                                              edge["short_len"]),
                                                label=[True, _short, edge["short_end"], edge["short_len"]])
                        else:
                            self.graph.add_edge("%s .E" % _short, "%s .B" % _long, overhang=abs(edge["long_start"] - 0),
                                                label=[True,_long, edge["long_start"], 0])
                            self.graph.add_edge("%s .E" % _long, "%s .B" % _short, overhang=abs(edge["short_end"] - 0),
                                                label=[True, _short, edge["short_end"], 0])
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s .B" % _long, "%s .B" % _short, overhang=abs(edge["short_start"] - 0),
                                                label=[True, _short, edge["short_start"], 0])
                            self.graph.add_edge("%s .E" % _short, "%s .E" % _long, overhang=abs(edge["long_end"] -
                                                                                              edge["long_len"]),
                                                label=[True, _long, edge["long_end"], edge["long_len"]])
                        else:
                            self.graph.add_edge("%s .B" % _long, "%s .E" % _short, overhang=abs(edge["short_len"] -
                                                                                              edge["short_start"]),
                                                label=[True, _short, edge["short_len"], edge["short_start"]])
                            self.graph.add_edge("%s .B" % _short, "%s .E" % _long, overhang=abs(edge["long_end"] -
                                                                                              edge["long_len"]),
                                                label=[True, _long, edge["long_end"], edge["long_len"]])

    def create_graph_v2(self):
        contained_nodes = set()
        for edge in self.edges_for_string_graph:
            if edge["contained"]:
                self.contained_edges.append(edge)
#                contained_nodes.add(edge["short_id"])
        for edge in self.edges_for_string_graph:
            _long = edge["long_id"]
            _short = edge["short_id"]
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes: 
                if not edge["contained"]:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s.+" % _long, "%s.+" % _short)
                            self.graph.add_edge("%s.-" % _short, "%s.-" % _long)
                        else:
                            self.graph.add_edge("%s.+" % _long, "%s.-" % _short)
                            self.graph.add_edge("%s.+" % _short, "%s.-" % _long)
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s.+" % _short, "%s.+" % _long)
                            self.graph.add_edge("%s.-" % _long, "%s.-" % _short)
                        else:
                            self.graph.add_edge("%s.-" % _short, "%s.+" % _long)
                            self.graph.add_edge("%s.-" % _long, "%s.+" % _short)

    def create_graph_v4(self):
        contained_nodes = set()
        for edge in self.edges_for_string_graph:
            if edge["contained"]:
                self.contained_edges.append(edge)
                #contained_nodes.add(edge["long_id"])
        for edge in self.edges_for_string_graph:
            edge = edge[0]
            _long = edge["long_id"]
            _short = edge["short_id"]
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes:
                if not edge["contained"]:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph2.add_edge(_long, _short, type=(True, True),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                            self.graph2.add_edge(_short, _long, type=(False, False),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                        else:
                            self.graph2.add_edge(_long, _short, type=(True, False),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                            self.graph2.add_edge(_short, _long, type=(True, False),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph2.add_edge(_short, _long, type=(True, True),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                            self.graph2.add_edge(_long, _short, type=(False, False),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                        else:
                            self.graph2.add_edge(_short, _long, type=(False, True),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])
                            self.graph2.add_edge(_long, _short, type=(False, True),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"])

    def create_graph_v5(self):
        contained_nodes = set()
        for edge in self.edges_for_string_graph:
            if edge["contained"]:
                self.contained_edges.append(edge)
                #contained_nodes.add(edge["long_id"])
        for edge in self.edges_for_string_graph:
            edge = edge[0]
            _long = edge["long_id"]
            _short = edge["short_id"]
            if str(_long) + ".T" not in self.graph2:
                self.graph2.add_node(str(_long) + ".T")
                self.graph2.add_node(str(_long) + ".F")
            if str(_short) + ".T" not in self.graph2:
                self.graph2.add_node(str(_short) + ".T")
                self.graph2.add_node(str(_short) + ".F")
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes:
                if not edge["contained"]:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph2.add_edge(str(_long) + ".T", str(_short) + ".T", type=(True, True),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                            self.graph2.add_edge(str(_short) + ".F", str(_long) + ".F", type=(False, False),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                        else:
                            self.graph2.add_edge(str(_long) + ".T", str(_short) + ".F", type=(True, False),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                            self.graph2.add_edge(str(_short) + ".T", str(_long) + ".F", type=(True, False),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph2.add_edge(str(_short) + ".T", str(_long) + ".T", type=(True, True),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                            self.graph2.add_edge(str(_long) + ".F", str(_short) + ".F", type=(False, False),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                        else:
                            self.graph2.add_edge(str(_short) + ".F", str(_long) + ".T", type=(False, True),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])
                            self.graph2.add_edge(str(_long) + ".F", str(_short) + ".T", type=(False, True),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])), short=edge["short_id"], short_ori=edge["reversed"], short_len=edge["short_len"], long_len=edge["long_len"], overlap_score=edge["overlap_score"])

    def create_bfs_tree(self, sources, bfs_tree, graph):
        new_sources = set()
        if len(sources) == 0:
            return bfs_tree
        for node, direction in sources:
            for neighbor, edge in graph[node].items():
                if edge["type"][0] == direction:
                    if neighbor not in bfs_tree.nodes():
                        new_sources.add((neighbor, edge["type"][1]))
                        bfs_tree.add_edge(node, neighbor)
                else:
                    continue
        return self.create_bfs_tree(new_sources, bfs_tree, graph)

    def paths_from_subgraph(self, nodes, rem_bet=False, lim=3):
        subgraph = self.graph2.subgraph(nodes)
        if rem_bet:
            bet = nx.betweenness_centrality(subgraph)
            sorted_bet = sorted([(x,y) for y,x in bet.items()])[::-1]
            _1perc = int(0.005 * len(sorted_bet))
            _ = [subgraph.remove_node(x[1]) for x in sorted_bet[:_1perc]]
            new_graph = nx.Graph(subgraph)
            max_nodes = max([(len(x),x) for x in nx.connected_components(new_graph)])[1]
            subgraph = subgraph.subgraph(nodes)
        cent = nx.degree_centrality(subgraph)
        sorted_cent = sorted([(x,y) for y,x in cent.items()])[::-1]
        i = np.argsort([x[0] for x in sorted_cent])[len(sorted_cent)//2]
        start_node = sorted_cent[i][1]
        tree = nx.DiGraph()
        t = self.create_bfs_tree([(start_node, True)], tree, subgraph)
        try:
            last_path = nx.dag_longest_path(tree)
        except ValueError:
            t = self.create_bfs_tree([(start_node, False)], tree, subgraph)
            last_path = nx.dag_longest_path(tree)
        plen = len(last_path)
        last_plen = 0 # int(plen) + 1
        while lim != 0:
            current_path = last_path
            if last_plen >= plen:
                lim -= 1
            tree = nx.DiGraph()
            plen = last_plen
            t2 = self.create_bfs_tree([(last_path[-1], True)], tree, subgraph)
            try:
                last_path = nx.dag_longest_path(tree)
            except ValueError:
                t2 = self.create_bfs_tree([(last_path[-1], False)], tree, subgraph)
                last_path = nx.dag_longest_path(tree)
            last_plen = len(last_path)
        return current_path

    def obtain_components(self):
        temp_graph = nx.Graph(self.graph2)
        return list(nx.connected_components(temp_graph))

    def create_graph_v3(self):
        for edge in self.edges_for_string_graph:
            _long = edge["long_id"]
            _short = edge["short_id"]
            if not edge["contained"]:
                if edge["long_start"] > 0:
                    if edge["short_start"] < edge["short_end"]:
                        self.graph.add_edge("%s.+" % _long, "%s.+" % _short, contained=False, short=_short)
                        self.graph.add_edge("%s.-" % _short, "%s.-" % _long, contained=False, short=_short)
                    else:
                        self.graph.add_edge("%s.+" % _long, "%s.-" % _short, contained=False, short=_short)
                        self.graph.add_edge("%s.+" % _short, "%s.-" % _long, contained=False, short=_short)
                else:
                    if edge["short_start"] < edge["short_end"]:
                        self.graph.add_edge("%s.+" % _short, "%s.+" % _long, contained=False, short=_short)
                        self.graph.add_edge("%s.-" % _long, "%s.-" % _short, contained=False, short=_short)
                    else:
                        self.graph.add_edge("%s.-" % _short, "%s.+" % _long, contained=False, short=_short)
                        self.graph.add_edge("%s.-" % _long, "%s.+" % _short, contained=False, short=_short)
            else:
                    if edge["short_start"] < edge["short_end"]:
                        self.graph.add_edge("%s.+" % _long, "%s.+" % _short, contained=True, short=_short)
                        self.graph.add_edge("%s.-" % _long, "%s.-" % _short, contained=True, short=_short)
                    else:
                        self.graph.add_edge("%s.+" % _long, "%s.-" % _short, contained=True, short=_short)
                        self.graph.add_edge("%s.+" % _long, "%s.-" % _short, contained=True, short=_short)

    def mark_reducable_edges(self):
        for node in self.graph.nodes():
            edges = [desc for desc in nx.dfs_edges(self.graph, node, depth_limit=1)]
            sorted_nodes = sorted([(self.graph.edges[x]["overhang"], x) for x in edges], reverse=True)
            for overlap_length, node_id in sorted_nodes:
                if self.graph.edges[node_id]["label"][0]:
                    nested_edges = list(nx.dfs_edges(self.graph, node_id[0], depth_limit=1))
                    for _, nested_node in nested_edges:
                        if nested_node in nx.dfs_edges(self.graph, node, depth_limit=1):
                            self.graph.edges[node][nested_node]["label"][0] = False

    def mark_spurious_edge(self):
        pass

    def show_graph(self, dot_pic_name='temp_dot.png'):
        import matplotlib.pyplot as plt
        from networkx.drawing.nx_agraph import write_dot
        from subprocess import check_call as ck
        write_dot(self.graph, "unfiltered_graph.dot")
        ck("dot -Tpng unfiltered_graph.dot -o %s%s" % (self.dot_pics_path, dot_pic_name), shell=True)
        # plt.imshow(plt.imread(self.dot_pics_path + dot_pic_name))
        # plt.show();plt.cla()


def fish_out_corr(id_pair: (int,int), correlation_stream):
    for corr, id_pair_ in correlation_stream:
        if id_pair[0] in id_pair_ and id_pair[1] in id_pair_:
            return corr, id_pair
    return None


def assemble_pair(corr: cs.CorrelationStruct, plot=False):
    if corr.short_overlap[0] == 0:
        if plot:
            assembly = np.concatenate((corr.new_long[:int(corr.long_overlap[0])], corr.new_short[:]))
            short = np.zeros(assembly.shape)
            long = np.array(short)
            long[:corr.new_long.shape[0]] = corr.new_long[:]
            short[corr.long_overlap[0]:] = corr.new_short[:]
            return assembly, long, short, corr
        return np.concatenate((corr.new_long[:int(min(corr.long_overlap))], corr.new_short[:]))
    else:
        if plot:
            assembly = np.concatenate((corr.new_short[:int(corr.short_overlap[0])], corr.new_long[:]))
            short = np.zeros(assembly.shape)
            long = np.array(short)
            short[:corr.new_short.shape[0]] = corr.new_short[:]
            long[corr.short_overlap[0]:] = corr.new_long[:]
            return assembly, long, short, corr
        return np.concatenate((corr.new_short[:int(min(corr.short_overlap))], corr.new_long[:]))


def make_corr_pairs_from_molecules(mol1: np.ndarray, mol2: np.ndarray):
    mol1 = ms.MoleculeNoBack(mol1, SNR)
    mol2 = ms.MoleculeNoBack(mol2, SNR)
    corr = cs.CorrelationStruct(mol1, mol2)
    if corr.fit_for_cross:
        corr.correlate_with_zoom((0.98, 1.02))
        return corr
    else:
        raise ValueError


def find_and_assemble_pair(pair: (int, int), corrs: [cs.CorrelationStruct, (int, int)]):
    corr, real_pair = fish_out_corr(pair, corrs)
    return assemble_pair(corr)


def assembly_initiator(path: [int], corrs):
    new_path = list()
    for i in range(0, len(path) - 1, 1):
        id_pair = (path[i], path[i+1])
        new_path.append(find_and_assemble_pair(id_pair, corrs))
    return new_path


def recursively_assemble_from_path(path: [np.ndarray]):
    if len(path) == 1:
        return path[0]
    else:
        new_path = list()
        for i in range(0, len(path)-1, 1):
            new_path.append(assemble_pair(make_corr_pairs_from_molecules(path[i], path[i+1])))
    return recursively_assemble_from_path(new_path)


def assemble_from_path(path: [np.ndarray]):
    if len(path) == 1:
        return path[0]
    else:
        head_assembly = assemble_pair(make_corr_pairs_from_molecules(path[0], path[1]))
        return assemble_from_path([head_assembly] + path[2:])


def generate_graph_edges(correlation_stream):
    for correlation, id_pair in correlation_stream:
        yield correlation_to_edge(correlation, id_pair)


def generate_graph_edges_v2(correlation_stream):
    for correlation, id_pair in correlation_stream:
        yield corr_info_to_edge(correlation, id_pair)


def correlation_to_edge(correlation: cs.CorrelationStruct, id_pair: tuple) -> dict:
    edge = dict()
    edge["overlap_score"] = correlation.max_score
    if correlation.long_id:
        edge["long_id"] = id_pair[0]
        edge["short_id"] = id_pair[1]
    else:
        edge["long_id"] = id_pair[1]
        edge["short_id"] = id_pair[0]
    edge["long_start"] = correlation.long_overlap[0]
    edge["long_end"] = correlation.long_overlap[1]
    if not correlation.reversed:
        edge["short_start"] = correlation.short_overlap[0]
        edge["short_end"] = correlation.short_overlap[1]
    else:
        edge["short_start"] = int(correlation.new_short_length - correlation.short_overlap[0])
        edge["short_end"] = int(correlation.new_short_length - correlation.short_overlap[1])
    edge["long_len"] = correlation.new_long_length
    edge["short_len"] = correlation.new_short_length
    if edge["short_len"] == abs(edge["short_end"] - edge["short_start"]):
        edge["contained"] = True
    else:
        edge["contained"] = False
    edge["reversed"] = correlation.reversed
    if edge["long_id"] != edge["short_id"]:
        return edge


def corr_info_to_edge(correlation: cs.CorrelationStruct, id_pair: tuple, dtype=DTYPE) -> dict:
    # overlap_score, long_id, short_id, long_start, long_end, short_start, short_end, long_lenx, short_len, contained, reversed
    edge = np.zeros(1, dtype=dtype)
    edge["overlap_score"][0] = correlation.max_score
    if correlation.long_id:
        edge["long_id"][0] = id_pair[0]
        edge["short_id"][0] = id_pair[1]
    else:
        edge["long_id"][0] = id_pair[1]
        edge["short_id"][0] = id_pair[0]
    edge["long_start"][0] = correlation.long_overlap[0]
    edge["long_end"][0] = correlation.long_overlap[1]
    if not correlation.reversed:
        edge["short_start"][0] = correlation.short_overlap[0]
        edge["short_end"][0] = correlation.short_overlap[1]
    else:
        edge["short_start"][0] = int(correlation.new_short_length - correlation.short_overlap[0])
        edge["short_end"][0] = int(correlation.new_short_length - correlation.short_overlap[1])
    edge["long_len"][0] = correlation.new_long_length
    edge["short_len"][0] = correlation.new_short_length
    if edge["short_len"][0] == abs(edge["short_end"][0] - edge["short_start"][0]):
        edge["contained"][0] = True
    else:
        edge["contained"][0] = False
    edge["reversed"][0] = correlation.reversed
    if edge["long_id"][0] != edge["short_id"][0]:
        return edge.flatten()