
import numpy as np
from numba import vectorize, jit
from scipy import fftpack, signal


def return_power_fft(signals, power_spectra_length=256):
    res = np.zeros((len(signals), power_spectra_length//2+1), dtype=float)
    for i in range(len(signals)):
        s = signals[i]
        res[i] = signal.welch(s, nperseg=power_spectra_length)[1]
    return res, signal.welch(s, nperseg=power_spectra_length)[0]


def choose_frequencies(frequency_list, signal_fft, signal_freq):
    res = np.zeros(frequency_list.shape)
    for i in range(res.shape[0]):
        current_freq = frequency_list[i]
        nearest_freq_idx = np.argmin(np.abs(current_freq - signal_freq))
        res[i] = signal_fft[nearest_freq_idx]
    return res


def filter_out_freqs(frequency_list, sig):
    sig_rfft = fftpack.rfft(sig)
    sig_size = sig.size
    freq = fftpack.rfftfreq(sig_size)
    return choose_frequencies(frequency_list, sig_rfft, freq)
