import numpy as np
from scipy import ndimage, fftpack
from numba import jit, vectorize, prange
from OptiMap import molecule_struct as ms
import math
from OptiMap import correlation_struct as cs
from scipy.cluster.vq import kmeans2, whiten
import h5py


class CrossCorrInput:
    def __init__(self, molecules: list, number_of_threads: int, dtype=np.float64, trunct=1024, to_log=False, spectra_ratio=0.5):
        self.trunct = trunct
        self.lens = return_lens(molecules)
        self.dtype = dtype
        self.cell_size = dtype().nbytes
        if to_log:
            log_mols = list()
            for mol in molecules:
                mol = np.where(mol <0, 0, mol)
                lmol = np.log1p(mol)
                lmol = np.where(lmol > (1.2*np.median(lmol)), lmol, 0.0001)
                log_mols.append(lmol)
            molecules = log_mols
        molecules = list(remove_spectra(molecules, ratio=spectra_ratio))
        self.molecules_fft = list_to_array(molecules, self.lens, trunct=self.trunct)
        self.molecules_fft_rev = list_to_array([x[::-1] for x in molecules], self.lens, trunct=self.trunct)
        self.threads = number_of_threads
        self.number_of_molecules = len(molecules)
        self.molecule_groups = list()

    def split_to_thread_jobs(self):
        self.molecule_groups = list()
        molecule_set = set(range(self.number_of_molecules))
        set_per_thread = self.number_of_molecules//self.threads
        for i in range(self.threads):
            thread_molecule_set = np.random.choice(list(molecule_set), size=set_per_thread, replace=False)
            molecule_set -= set(thread_molecule_set)
            self.molecule_groups.append(thread_molecule_set)

    def split_to_thread_jobs_in_sequence(self, proportionate=False):
        self.molecule_groups = list()
        set_per_tread = self.number_of_molecules//self.threads
        if not proportionate:
            for i in range(self.threads):
                self.molecule_groups.append(list(range(i*set_per_tread, i*set_per_tread+set_per_tread)))
        else:
            raw_proportions = np.arange(self.number_of_molecules)[::-1]
            total_space = np.sum(raw_proportions)
            proportions = raw_proportions/total_space
            r = 0
            thread_prop = 1/self.threads
            for i in range(self.threads):
                current_mollist = list()
                current_total = 0
                for mol_id in range(r, self.number_of_molecules, 1):
                    current_total += proportions[mol_id]
                    current_mollist.append(mol_id)
                    if current_total >= thread_prop or mol_id == (self.number_of_molecules - 1):
                        self.molecule_groups.append(current_mollist)
                        r = mol_id + 1
                        break
                    else:
                        continue


class CrossCorrMemmap:
    def __init__(self, cross_corr_data: CrossCorrInput, memmap_path: str, init=False):
        self.input = cross_corr_data
        self.memmap_path = memmap_path
        self.memmap = None
        self.maxes = None
        if init:
            self._initiate_memmap()
        self.indices = create_shifts(self.input.cell_size, self.input.number_of_molecules+1)
        self.black_listed = None

    def _initiate_memmap(self):
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype,
                                mode="w+", shape=(self.input.number_of_molecules**2))
        del memmap

    def insert_value_to_memmap(self, value: np.float64, index: tuple):
        shift = self._index_to_shift(index)
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r+", offset=shift, shape=1)
        memmap[0] = value
        del memmap

    def insert_array_to_memmap(self, array: np.ndarray, index: tuple):
        assert index[1] == 0
        shift = self._index_to_shift(index)
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r+", offset=int(shift), shape=array.shape)
        memmap[:] = array
        del memmap

    def insert_molecule_chunk_to_memmap(self, array: np.ndarray, mol_start: int):
        shift = self._index_to_shift((mol_start, 0))
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r+", offset=int(shift), shape=array.shape)
        memmap[:] = array
        del memmap

    def return_cross_corr_matrix(self, mol_range: tuple):
        shift = self._index_to_shift((mol_range[0], 0))
        sorted_range = np.sort(mol_range)
        shape = (sorted_range[-1] - sorted_range[0], self.input.number_of_molecules)
        return np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r", offset=int(shift), shape=shape)

    def sort_partial_matrix(self, mol_range: tuple, top=10, normalize=False, bl=False):
        mem = self.return_cross_corr_matrix(mol_range)
        if normalize:
            arr = np.array(mem)
            mat = np.zeros(arr.shape)
            normalize_matrix(arr, self.maxes, mat, mol_range[0])
        else:
            mat = np.array(mem)
        del mem
        if bl:
            mat[:, self.black_listed] = 0.000000001
        partial = np.zeros((mat.shape[0], top))
        argsort_into_partial_array(mat, partial)
        return partial[:, ::-1]

    def sort_complete_matrix(self, top=10, normalize=False, bl=False):
        all_sorted = np.zeros((self.input.number_of_molecules, top))
        if normalize:
            self.maxes = self.obtain_maxes((0, self.input.number_of_molecules))
        for i in range(0, self.input.number_of_molecules, 100):
            partial = self.sort_partial_matrix((i, i+100), top=top, normalize=normalize, bl=bl)
            all_sorted[i:i+100, :] = partial
        return all_sorted

    def obtain_maxes(self, mol_range: tuple):
        res = np.zeros(self.input.number_of_molecules)
        for i in range(mol_range[0], mol_range[1], 1):
            temp_mem = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r", offset=int(self._index_to_shift((i,0))), shape=res.shape[0])
            res[i] = np.max(temp_mem)
            del temp_mem
        return res

    def correlate_molecule_and_insert_to_memmap(self, mol_id):
        memmap_index = (mol_id, mol_id)
        self.insert_array_to_memmap(self.cross_corr_with_difference(mol_id), memmap_index)

    def correlate_chunk_and_insert_to_memmap(self, mol_group):
        start_id = mol_group[0]
        end_id = mol_group[-1] + 1
        print(start_id, end_id)
        chunk = np.zeros(int((self._index_to_shift((end_id, 0))//self.input.cell_size) -
                         (self._index_to_shift((start_id, 0))//self.input.cell_size))).reshape((len(mol_group), -1))
        current_index = 0
        for mol_id in mol_group:
            cross_result = self.cross_corr_with_difference(mol_id)
            chunk[current_index] = cross_result
            current_index += 1
        self.insert_molecule_chunk_to_memmap(chunk, start_id)
        del chunk

    def cross_corr_with_difference(self, mol_id):
        forward_corr = np.max(fftpack.ifft(self.input.molecules_fft * self.input.molecules_fft[mol_id], n=self.input.trunct).real, axis=1)
        reverse_corr = np.max(fftpack.ifft(self.input.molecules_fft * self.input.molecules_fft_rev[mol_id], n=self.input.trunct).real, axis=1)
        combined = cross_corr_difference(forward_corr, reverse_corr)
        return combined

    def run_thread(self, thread_id):
        self.ids_to_corrs(self.input.molecule_groups[thread_id])

    def ids_to_corrs(self, id_list):
        for mol_id in id_list:
            self.correlate_molecule_and_insert_to_memmap(mol_id)

    def _index_to_shift(self, index: tuple):
        cell_size = float(self.input.cell_size)
        return index[0] * (cell_size * self.input.number_of_molecules) + (index[1] * cell_size)

    
def remove_spectra(molecules, ratio=0.3):
    for mol in molecules:
        mollen = mol.shape[0]
        new_len = int(mollen*ratio)
        new_mol = np.zeros(new_len, dtype="complex")
        fft_mol = fftpack.rfft(mol)
        new_mol[:] = fft_mol[:new_len]
        yield fftpack.irfft(new_mol.real)


@jit
def diff_entropy(matrix):
    res = np.zeros(matrix.shape[0])
    for i in range(matrix.shape[0]):
        mol_hist = np.histogram(matrix[i],bins=10)[0]
        res[i] = -(mol_hist[0]*np.log1p(np.abs(mol_hist[0]))).sum()
    return res


@jit
def create_shifts(cell_size: int, number_of_molecules: int):
    indices = np.arange(number_of_molecules) * cell_size
    result = np.zeros(number_of_molecules)
    current_result = 0
    for i in range(number_of_molecules):
        current_result += indices[i]
        result[i] = current_result
    return result


@vectorize("float64(float64, float64)")
def cross_corr_difference(forward_corr, reverse_corr):
    if forward_corr >= reverse_corr:
        return forward_corr + (forward_corr - reverse_corr)
    else:
        return reverse_corr + (reverse_corr - forward_corr)


@vectorize("float64(float64,float64)")
def divide_vec(a, b):
    return a/b


def one_vs_all(single_molecule: np.ndarray, rest: np.ndarray):
    assert len(single_molecule.shape) == 1
    single_molecule_2d = np.zeros((1, 2*single_molecule.shape[0]))
    single_molecule_2d[0, :single_molecule.shape[0]] = single_molecule
    corr = ndimage.correlate(rest, single_molecule_2d, mode="constant")
    return np.max(corr, axis=1)


@jit
def return_lens(arr):
    res = np.zeros(len(arr), dtype=int)
    for i in range(len(arr)):
        res[i] = arr[i].shape[0]
    return res
        
@jit
def list_to_array(list_of_mols, lens, rev=False, trunct=1024):
    max_len = np.max(lens)
    result = np.zeros((len(list_of_mols), max_len*2))
    for i in range(len(list_of_mols)):
        if rev:
            result[i, :list_of_mols[i].shape[0]] = list_of_mols[i][::-1]
        else:
            result[i, :list_of_mols[i].shape[0]] = list_of_mols[i]
    return fftpack.fft(result, n=trunct)


def all_vs_all(all_molecules, max_molecule_len):
    all_molecules = list_to_array(all_molecules, max_molecule_len, rev=False)
    pairwise_matrix = np.zeros((all_molecules.shape[0]**2))
    for i in range(all_molecules.shape[0]):
        shift = i * all_molecules.shape[0]
        one_to_all = one_vs_all(all_molecules[i], all_molecules)
        rev_to_all = one_vs_all(all_molecules[i][::-1], all_molecules)
        one_to_all = np.where(one_to_all > rev_to_all, np.abs(one_to_all - rev_to_all) + one_to_all,
                              np.abs(one_to_all - rev_to_all) + rev_to_all)
        max_corr = np.max(one_to_all)
        one_to_all *= 100
        one_to_all /= max_corr
        pairwise_matrix[shift: shift + all_molecules.shape[0]] = one_to_all/np.max(one_to_all)
    return pairwise_matrix


def normalize_and_obtain_centroids(unnormalized_matrix, centroid_number=100):
    whitened = whiten(unnormalized_matrix)
    c, l = kmeans2(whitened, centroid_number)
    return whitened, c
   
@jit
def all_vs_all_distances_v2(normalized_matrix: np.ndarray, centroids):    
    centroid_similarities = np.zeros((normalized_matrix.shape[0], centroids.shape[0]), dtype=float)
    for i in range(centroid_similarities.shape[0]):
        for ii in range(centroid_similarities.shape[1]):
            centroid_similarities[i,ii] = np.mean(np.abs(normalized_matrix[i] - centroids[ii]))
    number_of_molecules = centroid_similarities.shape[0]
    all_ssqs = np.zeros(number_of_molecules, dtype=np.float64)
    for i in range(number_of_molecules):
        all_ssqs[i] = get_ssq(centroid_similarities[i], centroids.shape[0])
    distance_matrix = np.zeros((number_of_molecules, number_of_molecules))
    for i in range(number_of_molecules):
        for ii in range(0, number_of_molecules, 1):
            distance_matrix[i, ii] = sum_of_sqrs(centroid_similarities[i], centroid_similarities[ii], all_ssqs[i], all_ssqs[ii], centroids.shape[0])
    return distance_matrix

@jit
def all_vs_all_distances(pairwise_matrix: np.ndarray, number_of_molecules, limit=100):
    entropies = np.argsort(diff_entropy(pairwise_matrix))[::-1][:limit]
    filtered_matrix = pairwise_matrix[:,entropies]
    all_ssqs = np.zeros(number_of_molecules, dtype=np.float64)
    for i in range(number_of_molecules):
        all_ssqs[i] = get_ssq(filtered_matrix[i], limit)
    distance_matrix = np.zeros((number_of_molecules, number_of_molecules))
    for i in range(number_of_molecules):
        for ii in range(0, number_of_molecules, 1):
            distance_matrix[i, ii] = sum_of_sqrs(filtered_matrix[i], filtered_matrix[ii], all_ssqs[i], all_ssqs[ii], limit)
    return distance_matrix

@jit
def all_vs_all_distances_filtered(filtered_matrix: np.ndarray, number_of_molecules, limit=100):
    all_ssqs = np.zeros(number_of_molecules, dtype=np.float64)
    for i in range(number_of_molecules):
        all_ssqs[i] = get_ssq(filtered_matrix[i], limit)
    distance_matrix = np.zeros((number_of_molecules, number_of_molecules))
    for i in range(number_of_molecules):
        for ii in range(0, number_of_molecules, 1):
            distance_matrix[i, ii] = sum_of_sqrs(filtered_matrix[i], filtered_matrix[ii], all_ssqs[i], all_ssqs[ii], limit)
    return distance_matrix

@jit
def get_ssq(mol, length):
    ssq = 0
    for i in range(length):
        ssq += mol[i]**2
    return ssq

@jit
def get_ssq_cross(mol1, mol2, length):
    ssq = 0
    for i in range(length):
        ssq += mol1[i]*mol2[i]
    return ssq
@jit
def sum_of_sqrs(current_mol, second_mol, current_mol_ssq, second_mol_ssq, length):
    cross = get_ssq_cross(current_mol, second_mol, length)
    cross_ssq = math.sqrt(current_mol_ssq*second_mol_ssq)
    return cross/cross_ssq


@jit
def get_bidirectional_edges(distance_matrix: np.ndarray, hits=10):
    result = np.zeros((distance_matrix.shape[0], hits))
    result[:,:] = -1
    sorted_matrix = sort_matrix(distance_matrix)
    for i in range(distance_matrix.shape[0]):
        ids = sorted_matrix[i][:hits]
        for r in range(len(ids)):
            if i in sorted_matrix[ids[r]][:hits]:
                result[i,r] = ids[r]
    return result


@jit
def normalized_correlation(original_molecules, bidirectional_results: np.ndarray):
    results = list()
    for i in range(bidirectional_results.shape[0]):
        mol1 = ms.MoleculeNoBack(original_molecules[i], snr=3)
        logmol1 = ms.MoleculeNoBack(np.log1p(mol1.nick_signal), snr=3)
        for ii in range(bidirectional_results.shape[1]):
            if bidirectional_results[i,ii] != i and bidirectional_results[i,ii] != -1:
                mol2 = ms.MoleculeNoBack(original_molecules[int(bidirectional_results[i, ii])], snr=3)
                logmol2 = ms.MoleculeNoBack(np.log1p(mol2.nick_signal), snr=3)
                corr = cs.CorrelationStruct(mol1, mol2)
                corr.correlate_with_zoom((0.95, 1.05))
                logcorr = cs.CorrelationStruct(logmol1, logmol2)
                logcorr.correlate_with_zoom((0.95, 1.05))
                results.append((logcorr.max_score, corr.max_score, logcorr, corr))
            else:
                pass
    return results


@jit
def sort_matrix(matrix):
    result = np.zeros(matrix.shape)
    for i in range(matrix.shape[0]):
        result[i] = np.argsort(matrix[i])[::-1]
    return result.astype(int)


@jit(nopython=True, parallel=True)
def clear_molecules(new_matrix, mol_indices):
    for i in prange(mol_indices.shape[0]):
        molid = mol_indices[i]
        new_matrix[molid,:] = 0.0000001
        new_matrix[:,molid] = 0.0000001


@jit(nopython=True, parallel=True)
def make_new_array(old, zeros):
    for i in prange(old.shape[0]):
        zeros[i,:] = old[i]


@jit(nopython=True, parallel=True)
def normalize_matrix(matrix, max_values, normalized_matrix, shift):
    for i in prange(matrix.shape[0]):
        for j in prange(matrix.shape[1]):
            normalized_matrix[i,j] = matrix[i,j]/(math.sqrt(max_values[i+shift]*max_values[j]))


@jit(nopython=True, parallel=True)
def argsort_numba(matrix, argsorted):
    for i in prange(matrix.shape[0]):
        argsorted[i] = np.argsort(matrix[i])


@jit(nopython=True, parallel=True)
def argsort_into_partial_array(matrix, res):
    topnum = res.shape[1]
    for i in prange(matrix.shape[0]):
        res[i,:] = np.argsort(matrix[i])[-topnum:]


def corrs_from_graph(graph):
    pairs = []
    for edge in graph.edges():
        p1, p2 = (int(edge[0][:-2]), int(edge[1][:-2]))
        pairs.append((p1, p2))
    return pairs


def get_pairs_in_range(argsorted_array, r):
    pairs = []
    for mol_id1 in range(argsorted_array.shape[0]):
        for mol_id2 in argsorted_array[mol_id1][::-1][r[0]:r[1]]:
            if (mol_id1 == mol_id2):
                continue
            pairs.append((int(mol_id1), int(mol_id2)))
    return pairs


def set_graph_from_edges(pairs):
    nodes = set()
    for p1, p2 in pairs:
        nodes.add(p1)
        nodes.add(p2)
    set_graph = dict.fromkeys(nodes)
    [set_graph.update({x: set()}) for x in set_graph]
    for p1, p2 in pairs:
        set_graph[p1].add(p2)
        set_graph[p2].add(p1)
    return set_graph


def increase_graph_density(set_graph, depth=1):
    from itertools import combinations
    if depth == 0:
        print("done")
        return set_graph
    else:
        depth -= 1
        extension = dict.fromkeys(set_graph.keys())
        [extension.update({x: set()}) for x in extension]
        for pnode in set_graph:
            for p1, p2 in combinations(set_graph[pnode], 2):
                try:
                    extension[p1].add(p2)
                except KeyError:
                    pass
                try:
                    extension[p2].add(p1)
                except KeyError:
                    pass
        for pnode in extension:
            set_graph[pnode] = extension[pnode] | set_graph[pnode]
        return increase_graph_density(set_graph, depth)


def increase_graph_density_extender(set_graph, depth=1):
    from itertools import combinations
    if depth == 0:
        return None
    else:
        depth -= 1
        extension = dict.fromkeys(set_graph.keys())
        [extension.update({x: set()}) for x in extension]
        for pnode in set_graph:
            for p1, p2 in combinations(set_graph[pnode], 2):
                try:
                    extension[p1].add(p2)
                except KeyError:
                    extension[p1] = set([p2])
                try:
                    extension[p2].add(p1)
                except KeyError:
                    extension[p2] = set([p1])
        for pnode in extension:
            try:
                set_graph[pnode] = extension[pnode] | set_graph[pnode]
            except KeyError:
                set_graph[pnode] = set(list(extension[pnode]))
        return increase_graph_density_extender(set_graph, depth)


def get_pairs_from_graph(set_graph):
    pairs = list()
    for node in set_graph:
        for cnode in set_graph[node]:
            pairs.append((node, cnode))
    return pairs


def get_pairs(argsorted_array, depth):
    pairs = []
    for mol_id1 in range(argsorted_array.shape[0]):
        for mol_id2 in argsorted_array[mol_id1][-depth:]:
            if (mol_id1 == mol_id2):
                continue
            pairs.append((int(mol_id1), int(mol_id2)))
    return pairs


def normalized_cross_corr(mols, log_mols, pairs, limit, lp=0.8, cp=0.7):
    corrs = []
    n = 0
    for molid_1, molid_2 in pairs[:limit]:
        n += 1
        molid_1 = int(molid_1)
        molid_2 = int(molid_2)
        mol1 = ms.MoleculeNoBack(mols[molid_1], 4)
        mol2 = ms.MoleculeNoBack(mols[molid_2], 4)
        lmol1 = ms.MoleculeNoBack(mols[molid_1], 4)
        lmol2 = ms.MoleculeNoBack(mols[molid_2], 4)
        lmol1.nick_signal = log_mols[molid_1]
        lmol2.nick_signal = log_mols[molid_2]
        lcorr = cs.CorrelationStruct(lmol1, lmol2)
        lcorr.correlate_with_zoom((0.98, 1.02))
        corr = cs.CorrelationStruct(mol1, mol2)
        corr.correlate_with_zoom((1, 1))
        if n % 1000 == 0:
            print("%s percent is done" % (n / len(pairs) * 100))
            print("number of corrs: %s" % len(corrs))
        if lcorr.max_score >= lp and corr.max_score >= cp:
            corrs.append((lcorr, molid_1, molid_2))
    return corrs


def pairs_from_corrs(_corrs):
    return [(y, z) for x, y, z in _corrs]


def remove_nodes(graph, node_ids):
    for node in node_ids:
        del graph[node]
    return graph


def normalized_cross_corr_logs(log_mols, pairs, limit, lp=0.7):
    corrs = []
    n = 0
    for molid_1, molid_2 in pairs[:limit]:
        n += 1
        molid_1 = int(molid_1)
        molid_2 = int(molid_2)
        lmol1 = ms.MoleculeNoBack(log_mols[molid_1], 4)
        lmol2 = ms.MoleculeNoBack(log_mols[molid_2], 4)
        lcorr = cs.CorrelationStruct(lmol1, lmol2, bylength=250)
        lcorr.correlate_with_zoom((1, 1))
        if n % 1000 == 0:
            print("%s percent is done" % (n / len(pairs) * 100))
            print("number of corrs: %s" % len(corrs))
        if lcorr.max_score >= lp:
            corrs.append((lcorr, (molid_1, molid_2)))
    return corrs


def is_repeat(_mol):
    result = ndimage.gaussian_filter1d(np.correlate(_mol, _mol[::-1], mode='same'), sigma=1.5)
    mol_ = ms.MoleculeNoBack(result, snr=1.5)
    if len(mol_.nick_coordinates) < 6:
        return False
    nicks = np.array(mol_.nick_coordinates[0:6])
    diffs = np.abs(nicks[:5] - nicks[1:6])
    if (np.max(diffs) - np.min(diffs)) <= 2 and np.mean(diffs) >= 10:
        return True
    else:
        return False


def to_log_mols(mols):
    log_mols = list()
    for mol in mols:
        mol = np.where(mol <0, 0, mol)
        lmol = np.log1p(mol)
        lmol = np.where(lmol > (1.2*np.median(lmol)), lmol, 0.0001)
        log_mols.append(lmol)
    return log_mols


def split_pairs_to_threads(pairs, thread_num):
    total_len = len(pairs)
    pair_per_thread = total_len//thread_num
    left_over = total_len - (thread_num * pair_per_thread)
    res = []
    for i in range(0, thread_num * pair_per_thread, pair_per_thread):
        res.append(pairs[i:i+pair_per_thread])
    res.append(pairs[-left_over:])
    return res


def obtain_assembly_corrs(mols, log_mols, memmap_class: CrossCorrMemmap, t=4):
    import multiprocessing as mp
    from time import sleep
    from os import listdir
    # obtain normalized and sorted matrix
    argsorted_array = memmap_class.sort_complete_matrix(top=10, normalize=True)
    # obtain pairs from sorted matrix
    seed_pairs = get_pairs(argsorted_array, depth=10)
    # findout used corrs and unused corrs
    corrs = normalized_cross_corr(mols, log_mols, seed_pairs, len(seed_pairs), lp=0.8, cp=0.7)
    corr_pairs = pairs_from_corrs(corrs)
    corr_graph = set_graph_from_edges(corr_pairs)
    nodes_used = set(list(corr_graph.keys()))
    nodes_not_used = set(list(range(len(mols)))) - nodes_used
    # create black list and later on filter them out
    memmap_class.black_listed = np.array(list(nodes_not_used), dtype=int)
    argsorted_array = memmap_class.sort_complete_matrix(top=10, normalize=True, bl=True)
    corr_pairs = get_pairs(argsorted_array, 10)
    corr_pairs = [(p1, p2) for p1, p2 in corr_pairs if p1 in nodes_used and p2 in nodes_used]
    gr = set_graph_from_edges(corr_pairs)
    increase_graph_density(gr, 1)
    graph_pairs = get_pairs_from_graph(gr)

    def infunct_alignment(c):
        np.save("corrs/%s_%s.npy" % (c[0][0], c[-1][-1]), normalized_cross_corr(mols, log_mols, c, len(c), lp=0.8, cp=0.7))

    split_corrs = split_pairs_to_threads(graph_pairs, t)
    procs = [mp.Process(target=infunct_alignment, args=(x,)) for x in split_corrs]
    _ = [x.start() for x in procs]
    while sum([x.is_alive() for x in procs]) != 0:
        sleep(5)
        print([x.is_alive() for x in procs])
    res_corrs = []
    corr_files = ["corrs/" + x for x in listdir("corrs/")]
    _ = [[res_corrs.append(y) for y in np.load(x)] for x in corr_files]
    return res_corrs