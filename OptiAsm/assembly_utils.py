from dask.distributed import Client, LocalCluster
from OptiMap import molecule_struct as ms
from OptiMap import correlation_struct as cs
from OptiScan import utils, database
from OptiAsm.consensus import Consensus, optimize_consensus, nan_to_1
from OptiAsm import string_graph
import numpy as np
import numba as nb
from itertools import combinations

def return_single_corr(id1, id2, mol1, mol2, zoom=(0.99,1.01), thr1=0.6, thr2=0.7):
    mol1 = ms.MoleculeNoBack(mol1, 3.2)
    mol2 = ms.MoleculeNoBack(mol2, 3.2)
    corr = cs.CorrelationStruct(mol1, mol2)
    corr.correlate_with_zoom(zoom, log=False)
    if not corr.max_score or corr.max_score < thr1:
        return None
    else:
        corr = cs.CorrelationStruct(mol1, mol2)
        corr.correlate_with_zoom(zoom, log=True)
        if corr.max_score > thr2:
            return (corr, (id1, id2))
        else:
            return None

def return_single_corr_v2(id1, id2, mol1, mol2, zoom=(0.99,1.01), thr1=0.6, thr2=0.7, 
                          return_corr_signals=False):
    mol1 = ms.MoleculeNoBack(mol1, 3.2)
    mol2 = ms.MoleculeNoBack(mol2, 3.2)
    corr = cs.CorrelationStruct(mol1, mol2)
    corr.correlate_with_zoom(zoom, log=False)
    if not corr.max_score or corr.max_score < thr1:
        return None
    else:
        corr = cs.CorrelationStruct(mol1, mol2, return_new_signals=return_corr_signals)
        corr.correlate_with_zoom(zoom, log=True)
        if corr.max_score > thr2:
            return string_graph.corr_info_to_edge(corr, (id1, id2))
        else:
            return None

def send_pairs_to_clusters(pairs, mols, zoom=(0.99, 1.01), thr1=0.6, thr2=0.7, client_ip=None, 
                           steps=1000, return_corrs=True, return_corr_signals=False):
    if return_corrs:
        res = list()
    else:
        res = np.zeros((len(pairs),1), dtype=string_graph.DTYPE)
    prog = 0
    if not client_ip:
        cluster = LocalCluster()
        client = Client(cluster)
    else:
        client = Client(client_ip)

    def filt(x):
        if x:
            return True
        else:
            return False

    for i in range(0, len(pairs), steps):
        batch = list()
        current_pairs = pairs[i:i+steps]
        for pair in current_pairs:
            m1, m2 = pair
            batch.append(client.submit(return_single_corr_v2, m1, m2, mols[m1], mols[m2], zoom, thr1, thr2, return_corr_signals))
        if return_corrs:
            filtered_res = list( filter(filt, client.gather(batch)))
            res += filtered_res
            prog += len(filtered_res)
        else:
            filtered_res = np.array(list(filter(filt, client.gather(batch))), dtype=string_graph.DTYPE)
            if filtered_res.shape[0] == 0:
                continue
            else:
                res[prog:prog+filtered_res.shape[0]] = filtered_res
                prog += filtered_res.shape[0]
        print(i)
    return res[:prog]


def cons_to_array(c, mols):
    arr = np.zeros((len(c.space.all_intervals),max([x.end for x in c.space.all_intervals])+100), dtype=float)
    arr[:] = np.NaN
    i = 0
    c.space.all_intervals

    for start,stop,mol_id,flip in sorted([(x.begin,x.end,x.data[0],x.data[1]) for x in c.space.all_intervals if x.data != "consensus"]):
        if flip == False:
            m = mols[mol_id][::-1]
        else:
            m = mols[mol_id]
        try:
            arr[i,start:start+m.shape[0]] = m
            i += 1
        except:
            continue
    arr = optimize_consensus(arr,30)
    return arr


def get_consensus_for_paths(corrs, mols, paths):
    for num in range(len(paths)):
        c = Consensus(corrs)
        c.from_path_to_space2(paths[num][0],paths[num][1]),paths[num]
        path_mols = paths[num][0]
        c.fill_contained_space(path_mols,mols, containing=False)
        c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=True)
        c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=False)
        c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=True)
        c.fill_contained_space([x for x in list(c.index.keys()) if x not in path_mols],mols,containing=False)
        arr = cons_to_array(c, mols)
        yield arr


def get_trimmed_cons(cons, trim_ratio):
    import matplotlib.pyplot as plt
    for con in cons:
        idx = trim_arr(np.nan_to_num(con), trim_ratio).astype(int)
        plt.imshow(con)
        plt.show()
        plt.imshow(con[:,idx[0]:idx[1]])
        plt.show()
        yield con[:,idx[0]:idx[1]]


def get_median_cons(cons, q=55):
    for arr in cons:
        quart = np.nan_to_num(np.nanpercentile(arr, q,axis=0))
        yield quart


def pipe_contigs(corrs, mols, paths, trim_ratio=0.05):
    cons = get_consensus_for_paths(corrs, mols, paths)
    cons = get_trimmed_cons(cons, trim_ratio)
    cons = get_median_cons(cons)
    cons = list(cons)
    return cons

@nb.njit
def count_idx(arr, idx):
    n = 0.00000001
    for i in range(arr.shape[0]):
        if arr[i, idx-1]  == 0:
            continue
        else:
            n += 1
    return np.float(n)/np.float(100)

@nb.njit
def trim_left(arr, ratio):
    current_ratio = 0
    i = 0
    while current_ratio < ratio:
        current_ratio = count_idx(arr, i)
        i += 1
    return i

@nb.njit
def trim_right(arr, ratio):
    current_ratio = 0
    i = arr.shape[1]
    while current_ratio < ratio:
        current_ratio = count_idx(arr, i)
        i -= 1
    return i

@nb.njit
def trim_arr(arr, ratio):
    res = np.zeros(2)
    res[1] = trim_right(arr, ratio)
    res[0] = trim_left(arr, ratio)
    return res

def set_graph_from_edges(pairs):
    nodes = set()
    for p1,p2 in pairs:
        nodes.add(p1)
        nodes.add(p2)
    set_graph = dict.fromkeys(nodes)
    [set_graph.update({x:set()}) for x in set_graph]
    for p1,p2 in pairs:
        if p1 != p2:
            set_graph[p1].add(p2)
            set_graph[p2].add(p1)
        else:
            pass
    return set_graph


def increase_graph_density(set_graph, depth=1):
    if depth == 0:
        print("done")
        return set_graph
    else:
        depth -= 1
        extension = dict.fromkeys(set_graph.keys())
        [extension.update({x:set()}) for x in extension]
        for pnode in set_graph:
            for p1,p2 in combinations(set_graph[pnode], 2):
                try:
                    extension[p1].add(p2)
                except KeyError:
                    pass
                try:
                    extension[p2].add(p1)
                except KeyError:
                    pass
        for pnode in extension:
            set_graph[pnode] = extension[pnode] | set_graph[pnode]
        return increase_graph_density(set_graph, depth)


def increase_graph_density_extender(set_graph, depth=1):
    if depth == 0:
        return set_graph
    else:
        depth -= 1
        extension = dict.fromkeys(set_graph.keys())
        [extension.update({x:set()}) for x in extension]
        for pnode in set_graph:
            for p1,p2 in combinations(set_graph[pnode], 2):
                try:
                    extension[p1].add(p2)
                except KeyError:
                    extension[p1] = set([p2])
                try:
                    extension[p2].add(p1)
                except KeyError:
                    extension[p2] = set([p1])
        for pnode in extension:
            try:
                set_graph[pnode] = extension[pnode] | set_graph[pnode]
            except KeyError:
                set_graph[pnode] = set(list(extension[pnode]))
        return increase_graph_density_extender(set_graph, depth)


def get_pairs_from_graph(set_graph):
    pairs = list()
    for node in set_graph:
        for cnode in set_graph[node]:
            pairs.append((node, cnode))
    return pairs


def path_to_map(mols, out_folder, ref_aligner_path, cmap_file, bnx_header):
    bnx_file_name = "path_.bnx"
    database.molecules_to_bnxv2([(x,np.zeros(x.shape[0])) for x in mols], 10, 500, bnx_file_name, signal_to_noise_ratio=3, bnx_template_path=bnx_header)
    mqr = utils.MQR(out_folder, ref_aligner_path)
    mqr.run_ref_align(cmap_file, bnx_file_name, len(mols))
    mqr.load_results()
    xmp_self = mqr.xmap
    xmp_self.read_and_load_xmap_file()
    line_details = [xmp_self.xmap_line_details(line) for line in xmp_self.xmap_lines]
    return line_details