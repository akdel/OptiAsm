from subprocess import check_call as ck
from OptiScan import utils as bnx
from OptiAsm import string_graph as sg
import networkx as nx
from OptiAsm.assembly_utils import pipe_contigs


def get_score(edge_bet, bet_start_node, bet_end_node, bet_scores, edge_info, t=1000, rev=True):
    ov1,ov2 = edge_info["overlap"][0]
    overlap = abs(ov1 - ov2)
    overlap_score = edge_info["overlap_score"]
    udag1 = bet_start_node
    udag2 = bet_end_node
    try:
        edge_bet_score = edge_bet[(udag1,udag2)] + (bet_scores[udag1] + bet_scores[udag2])/2
    except KeyError:
        edge_bet_score = edge_bet[(udag2,udag1)] + (bet_scores[udag1] + bet_scores[udag2])/2
    if rev == "score":
        return -overlap_score
    elif rev:
        return -(overlap * overlap_score)
    else:
        return overlap
    

def add_weights(old_graph, edge_bet, bet_scores, t=1000, rev=True):
    new_dag_graph = nx.DiGraph()
    for node in old_graph.nodes():
        new_dag_graph.add_node(node)
    for node1,node2 in old_graph.edges():
        edge = old_graph[node1][node2]
        w = get_score(edge_bet, node1, node2, bet_scores, edge, t=t, rev=rev)
        new_dag_graph.add_edge(node1, node2, weight=w)
    return new_dag_graph

def bellman(graph, cutoff=None ,weight="weight"):
    res = []
    i = 0
    print(i)
    for n in graph:
        if i == 10:
            break
        try:
            res.append((n, nx.single_source_bellman_ford_path_length(graph, n, cutoff=cutoff, weight=weight)))
        except nx.NetworkXUnbounded:
            pass
        i += 1
    return res

def fix_node_names(nodes):
    node_names = []
    for node in nodes:
        node_names.append(str(node) + ".T")
        node_names.append(str(node) + ".F")
    return node_names

def obtain_orig_graph_from_path(path, graph):
    path = [int(x[:-2]) for x in path]
    g = graph.subgraph(path)
    return (path, g)


class PathFinder:
    def __init__(self, corrs, bet_thr=0.05, edge_bet=0.05, k_ratio=0.1, load_cutoff=5):
        self.corrs = corrs
        self.graph_type1 = self.return_primary_graph(t=1).graph2
        self.graph_type2 = nx.Graph(self.return_primary_graph(t=2).graph2)
        self.components = None
        self.return_components(bet_thr=bet_thr, edge_bet=edge_bet, load_cutoff=load_cutoff, k_ratio=k_ratio)
        self.contig_paths = {}

    def return_primary_graph(self, t=1):
        gr = sg.StringGraph(self.corrs)
        if t == 1:
            gr.create_graph_v5()
        else:
            gr.create_graph_v4()
        return gr

    def return_components(self, bet_thr=0.05, edge_bet=0.05, minimum_splitable_component=50, k_ratio=0.1, load_cutoff=5):
        gr2 = self.return_primary_graph(t=2)
        gr = self.return_primary_graph(t=1)
        comps = gr.obtain_components()
        paths = []
        undirected = nx.Graph(gr.graph2)
        current_k_ratio = k_ratio
        for comp in comps:
            if len(comp) > minimum_splitable_component:
                if len(comp) <= 1000:
                    current_k_ratio = 1
                else:
                    current_k_ratio = k_ratio
                print(len(comp))
                subgraph = undirected.subgraph(comp)
                bet = nx.load_centrality(subgraph, cutoff=load_cutoff)
                print("load found")
                sorted_bet = sorted([(x,y) for  y,x in bet.items()])[::-1]
                _01perc = int(len(sorted_bet)*bet_thr)
                _01lim = sorted_bet[_01perc][0]
                print("start chimera")
                _ = [(gr.graph2.remove_node(x), subgraph.remove_node(x),print(x)) for x,y in bet.items() if y > _01lim]
                print("end chimera")
                edge_dict = nx.edge_betweenness_centrality(subgraph, k=int(subgraph.number_of_nodes()*current_k_ratio))
                print("edge betweenness found")
                sorted_edge_dict = sorted([(x,y) for y,x in edge_dict.items()])[::-1]
                _01perc = int(len(sorted_edge_dict)*edge_bet)
                if _01perc == 0:
                    continue
                _01lim = sorted_edge_dict[_01perc][0]
                print(_01lim)
                _ = [(subgraph.remove_edge(x[0],x[1])) for x,y in edge_dict.items() if y > _01lim]
                for x,y in edge_dict.items():
                    if y > _01lim:
                        if x[0] == x[1]:
                            try:
                                subgraph.remove_node(x[0])
                            except nx.NetworkXError:
                                continue
                        else:
                            try:
                                subgraph.remove_node(x[0])
                            except nx.NetworkXError:
                                continue
                            try:
                                subgraph.remove_node(x[1])  
                            except nx.NetworkXError:
                                continue
                print(len(_))
                
                comp2s = list(nx.connected_components(subgraph))
                for comp2 in comp2s:
                    paths.append(comp2)
            else:
                paths.append(comp)
        self.components = paths

    def find_longest_path_in_component(self, component_id, limit=3, k_ratio=0.1, load_path_cutoff=5):
        sub_g = nx.DiGraph(self.graph_type1.subgraph(self.components[component_id]))
        bet = nx.load_centrality(sub_g, cutoff=load_path_cutoff)
        edge_dict = nx.edge_betweenness_centrality(sub_g, k=int(sub_g.number_of_nodes()*k_ratio))
        if sub_g.number_of_nodes() < limit:
            print("Number of nodes in component %s: %s" % (component_id, sub_g.number_of_nodes()))
            return []
        else:
            print("Number of nodes in component %s: %s" % (component_id, sub_g.number_of_nodes()))
            
            new_graph = add_weights(sub_g, edge_dict, bet, t=10000, rev=False)
            new_graph_rev = add_weights(sub_g, edge_dict, bet, t=10000, rev=True)
            # score_graph = add_weights(sub_g, edge_dict, bet, t=10000, rev="score")

            all_vs_all = nx.all_pairs_dijkstra_path_length(new_graph, weight="weight")
            l = []
            _ = [[l.append((i,x[0],y)) for y,i in x[1].items()] for x in all_vs_all.items()]
            _, node1, node2 = sorted(l)[::-1][0]
            return nx.astar_path(new_graph_rev, node1, node2, weight="weight")
    
    def find_paths_for_all_contigs(self, limit=3, k_ratio=0.1, load_path_cutoff=5):
        for i in range(len(self.components)):
            contig_path_candidate = self.find_longest_path_in_component(i, limit=limit, k_ratio=k_ratio)
            if len(contig_path_candidate) > 0:
                self.contig_paths[i] = contig_path_candidate
            else:
                continue

    def consensus_contigs(self, mols, corrs, contig_id):
        to_assemble = obtain_orig_graph_from_path(self.contig_paths[contig_id], self.graph_type2)
        return pipe_contigs(corrs, mols, [(to_assemble[0], self.graph_type2)])
