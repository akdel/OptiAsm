import numpy as np
from scipy import ndimage, fftpack
from numba import jit, vectorize
from OptiAsm import molecule_struct as ms
import math
from OptiAsm import correlation_struct as cs
from scipy.cluster.vq import kmeans2, whiten
import h5py
#fftpack = pyfftw.interfaces.scipy_fftpack # monkey patch


class CrossCorrInput:
    def __init__(self, molecules: list, number_of_threads: int, dtype=np.float64, trunct=1024):
        self.trunct = trunct
        self.lens = return_lens(molecules)
        self.dtype = dtype
        self.cell_size = dtype().nbytes
        self.molecules_fft = list_to_array(molecules, self.lens, trunct=self.trunct)
        self.molecules_fft_rev = list_to_array([x[::-1] for x in molecules], self.lens, trunct=self.trunct)
        self.threads = number_of_threads
        self.number_of_molecules = len(molecules)
        self.molecule_groups = list()

    def split_to_thread_jobs(self):
        self.molecule_groups = list()
        molecule_set = set(range(self.number_of_molecules))
        set_per_thread = self.number_of_molecules//self.threads
        for i in range(self.threads):
            thread_molecule_set = np.random.choice(list(molecule_set), size=set_per_thread, replace=False)
            molecule_set -= set(thread_molecule_set)
            self.molecule_groups.append(thread_molecule_set)

    def split_to_thread_jobs_in_sequence(self, proportionate=False):
        self.molecule_groups = list()
        set_per_tread = self.number_of_molecules//self.threads
        if not proportionate:
            for i in range(self.threads):
                self.molecule_groups.append(list(range(i*set_per_tread, i*set_per_tread+set_per_tread)))
        else:
            raw_proportions = np.arange(self.number_of_molecules)[::-1]
            total_space = np.sum(raw_proportions)
            proportions = raw_proportions/total_space
            r = 0
            thread_prop = 1/self.threads
            for i in range(self.threads):
                current_mollist = list()
                current_total = 0
                for mol_id in range(r, self.number_of_molecules, 1):
                    current_total += proportions[mol_id]
                    current_mollist.append(mol_id)
                    if current_total >= thread_prop or mol_id == (self.number_of_molecules - 1):
                        self.molecule_groups.append(current_mollist)
                        r = mol_id + 1
                        break
                    else:
                        continue


class CrossCorrMemmap:
    def __init__(self, cross_corr_data: CrossCorrInput, memmap_path: str, init=False):
        self.input = cross_corr_data
        self.memmap_path = memmap_path
        self.memmap = None
        if init:
            self._initiate_memmap()
        self.indices = create_shifts(self.input.cell_size, self.input.number_of_molecules+1)
        self.input.split_to_thread_jobs_in_sequence(True)
        self.representatives_exist = False
        self.representatives = None

    def _initiate_memmap(self):
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype,
                                mode="w+", shape=(self.input.molecules_fft.shape[0]**2)//2)
        del memmap

    def insert_value_to_memmap(self, value: np.float64, index: tuple):
        shift = self._index_to_shift(index)
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r+", offset=shift, shape=1)
        memmap[0] = value
        del memmap

    def load_representatives(self, input_class):
        self.representatives = input_class
        self.representatives_exist = True

    def cross_corr_for_against_representatives(self, mol_group, piece):
        assert self.representatives
        res = np.zeros((len(mol_group), self.representatives.molecules_fft.shape[0]), dtype=float)
        for i in range(len(mol_group)):
            res[i] = self.cross_corr_difference_against_representatives(mol_group[i])
        np.save("vsrep_%s.npy" % piece, res)

    def insert_array_to_memmap(self, array: np.ndarray, index: tuple):
        shift = self._index_to_shift(index)
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r+", offset=int(shift), shape=array.shape)
        memmap[:] = array
        del memmap

    def insert_molecule_chunk_to_memmap(self, array: np.ndarray, mol_start: int):
        shift = self._index_to_shift((mol_start, mol_start))
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r+", offset=int(shift), shape=array.shape)
        memmap[:] = array
        del memmap
        pass

    def return_cross_corr_matrix(self):
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r")
        f = h5py.File("test.hpy", "w")
        memmap_matrix = f.create_dataset("data", (self.input.number_of_molecules, self.input.number_of_molecules), dtype="f8")#np.memmap("./test100.mem", dtype=np.float32, shape=(self.input.number_of_molecules, self.input.number_of_molecules), mode="w+")
        array = None #np.zeros((self.input.number_of_molecules, self.input.number_of_molecules), dtype=np.float32)
        def shift():
            for i1 in range(0, self.input.number_of_molecules - 1, 1):
                yield (self._index_to_shift((i1,i1))//self.input.cell_size ,
                       self._index_to_shift((i1+1, i1+1))//self.input.cell_size, i1)
        from time import time
        t = time()
        for i, ii, r in shift():
            i = int(i)
            ii = int(ii)
            if r % 1000 == 0:
                print(time() - t, r)
                t = time()
                del memmap
                memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r")
            try:               
                memmap_matrix[r, r:] = memmap[i:ii]
            except:
                print(False)
                continue
        memmap_matrix[-1, -1] = memmap[-1]
        for i in range(data.shape[0]):
            memmap_matrix[i, :i] = memmap_matrix[:i, i]
        for i in range(data.shape[0]):
            m = np.max(memmap_matrix[i])
            memmap_matrix[i] /= m
        return None

    
    def mirror_array(self, hdf_file):
        f = h5py.File(hdf_file, "r+")
        data = f["data"]
        for i in range(data.shape[0]):
            data[i, :i] = data[:i, i]
        f.close()

    
    def load_partial_matrix(self, n):
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r")
        array = np.zeros((n, n), dtype=np.float64)
        def shift():
            for i1 in range(0, n - 1, 1):
                yield (self._index_to_shift((i1,i1))//self.input.cell_size ,
                       self._index_to_shift((i1+1, i1+1))//self.input.cell_size, i1)

        for i, ii, r in shift():
            i = int(i)
            ii = int(ii)
            try:
                if ii < n:
                    array[r, r:] = memmap[i:n]
                else:
                    array[r, r:] = memmap[i:n]
            except:
                continue
        array[-1, -1] = memmap[-1]
        for i in range(array.shape[0]):
            array[i, :i] = array[:i, i]
        for i in range(array.shape[0]):
            m = np.max(array[i])
            array[i] /= m
        return array
    
    def return_unnormalized_cross_corr_matrix(self):
        memmap = np.memmap(self.memmap_path, dtype=self.input.dtype, mode="r")
        array = np.zeros((self.input.number_of_molecules, self.input.number_of_molecules), dtype=np.float32)
        def shift():
            for i1 in range(0, self.input.number_of_molecules - 1, 1):
                yield (self._index_to_shift((i1,i1))//self.input.cell_size ,
                       self._index_to_shift((i1+1, i1+1))//self.input.cell_size, i1)

        for i, ii, r in shift():
            i = int(i)
            ii = int(ii)
            print(i,ii,r)
            print(array.shape, memmap[i:ii].shape)
            try:
                array[r, r:] = memmap[i:ii].astype("float32")
            except:
                continue
        array[-1, -1] = memmap[-1].astype("float32")
        for i in range(array.shape[0]):
            array[i, :i] = array[:i, i]
        return array

    def correlate_molecule_and_insert_to_memmap(self, mol_id):
        memmap_index = (mol_id, mol_id)
        self.insert_array_to_memmap(self.cross_corr_with_difference(mol_id), memmap_index)

    def correlate_chunk_and_insert_to_memmap(self, mol_group):
        from time import time
        start_id = mol_group[0]
        end_id = mol_group[-1] + 1
        chunk = np.zeros(int((self._index_to_shift((end_id, end_id))/self.input.cell_size) -
                         (self._index_to_shift((start_id, start_id))/self.input.cell_size)))
        print(chunk.shape)
        current_index = 0
        t = time()
        for mol_id in mol_group:
            cross_result = self.cross_corr_with_difference(mol_id)
            chunk[current_index: current_index + cross_result.shape[0]] = cross_result
            current_index += cross_result.shape[0]
        print(time() - t)
        self.insert_molecule_chunk_to_memmap(chunk, start_id)
        del chunk

    def delete_extra_fft(self, start):
        self.input.molecules_fft = self.input.molecules_fft[start:]
        self.input.molecules_fft_rev = self.input.molecules_fft_rev[start:] 


    def cross_corr_with_difference(self, mol_id):
        forward_corr = np.max(fftpack.ifft(self.input.molecules_fft[mol_id:] * self.input.molecules_fft[mol_id], n=self.input.trunct).real, axis=1)
        reverse_corr = np.max(fftpack.ifft(self.input.molecules_fft[mol_id:] * self.input.molecules_fft_rev[mol_id], n=self.input.trunct).real, axis=1)
        combined = cross_corr_difference(forward_corr, reverse_corr)
        return combined

    def cross_corr_difference_against_representatives(self, mol_id):
        forward_corr = np.max(fftpack.ifft(self.representatives.molecules_fft[:] * self.input.molecules_fft[mol_id],
                                           n=self.input.trunct).real, axis=1)
        reverse_corr = np.max(fftpack.ifft(self.representatives.molecules_fft[:] * self.input.molecules_fft_rev[mol_id],
                                           n=self.input.trunct).real, axis=1)
        combined = cross_corr_difference(forward_corr, reverse_corr)
        return combined

    def run_thread(self, thread_id):
        self.ids_to_corrs(self.input.molecule_groups[thread_id])

    def ids_to_corrs(self, id_list):
        for mol_id in id_list:
            self.correlate_molecule_and_insert_to_memmap(mol_id)

    def _index_to_shift(self, index: tuple):
        index = tuple(sorted(index))
        cell_size = float(self.input.cell_size)
        return (index[0] * (self.input.molecules_fft.shape[0] * cell_size)) - \
               (self.indices[index[0]] - (cell_size*index[0]))
    
    def create_sorted_array(self, hdf_file_name, mem_name, start=0, stop=20000, slice_size=1000):
        row_size = self.input.cell_size * self.input.number_of_molecules
        memmap = np.memmap(mem_name, dtype=self.input.dtype, mode="r+", shape=(self.input.number_of_molecules, self.input.number_of_molecules))
        del memmap
        f = h5py.File(hdf_file_name, "r")
        data = f["data"]
        for i in range(start, stop, slice_size):
            if i % 1000 == 0:
                print(i)
            memmap = np.memmap(mem_name, dtype=self.input.dtype, mode="r+",shape=(self.input.number_of_molecules, self.input.number_of_molecules)) #shape=(slice_size, self.input.number_of_molecules), offset=(row_size*i)*slice_size)
            data_slice = data[i:i+slice_size]
            print(data_slice.shape)
            print(data_slice[:100,:100])
            print(memmap.shape)
            print(memmap[:100,:100])
            memmap[i:i+slice_size] = np.argsort(data_slice).astype(float)
            del memmap
        f.close()
        pass


    def mirror_and_sort(self, hdf_file_name, mem_name, start_id=20000):
        row_size = self.input.cell_size * self.input.number_of_molecules
        memmap = np.memmap(mem_name, dtype=self.input.dtype, mode="r+", shape=(self.input.number_of_molecules, self.input.number_of_molecules))
        del memmap
        f = h5py.File(hdf_file_name, "r+")
        data = f["data"]
        for i in range(start_id, self.input.number_of_molecules):
            if i % 10 == 0:
                print(i)
            memmap = np.memmap(mem_name, dtype=self.input.dtype, mode="r+", shape=(self.input.number_of_molecules,), offset=(row_size*i))
            data[i, :i] = data[:i, i]
            data_slice = data[i]
            memmap[:] = np.argsort(data_slice)
            del memmap
        f.close()
        pass
    
def remove_spectra(molecules, ratio=0.3):
    for mol in molecules:
        mollen = mol.shape[0]
        new_len = int(mollen*ratio)
        new_mol = np.zeros(new_len, dtype="complex")
        fft_mol = fftpack.rfft(mol)
        new_mol[:] = fft_mol[:new_len]
        yield fftpack.irfft(new_mol.real)


@jit
def diff_entropy(matrix):
    res = np.zeros(matrix.shape[0])
    for i in range(matrix.shape[0]):
        mol_hist = np.histogram(matrix[i],bins=10)[0]
        res[i] = -(mol_hist[0]*np.log1p(np.abs(mol_hist[0]))).sum()
    return res


@jit
def create_shifts(cell_size: int, number_of_molecules: int):
    indices = np.arange(number_of_molecules) * cell_size
    result = np.zeros(number_of_molecules)
    current_result = 0
    for i in range(number_of_molecules):
        current_result += indices[i]
        result[i] = current_result
    return result


@vectorize("float64(float64, float64)")
def cross_corr_difference(forward_corr, reverse_corr):
    if forward_corr >= reverse_corr:
        return forward_corr + (forward_corr - reverse_corr)
    else:
        return reverse_corr + (reverse_corr - forward_corr)


@vectorize("float64(float64,float64)")
def divide_vec(a, b):
    return a/b


def one_vs_all(single_molecule: np.ndarray, rest: np.ndarray):
    assert len(single_molecule.shape) == 1
    single_molecule_2d = np.zeros((1, 2*single_molecule.shape[0]))
    single_molecule_2d[0, :single_molecule.shape[0]] = single_molecule
    corr = ndimage.correlate(rest, single_molecule_2d, mode="constant")
    return np.max(corr, axis=1)


@jit
def return_lens(arr):
    res = np.zeros(len(arr), dtype=int)
    for i in range(len(arr)):
        res[i] = arr[i].shape[0]
    return res
        
@jit
def list_to_array(list_of_mols, lens, rev=False, trunct=1024):
    max_len = np.max(lens)
    result = np.zeros((len(list_of_mols), max_len*2))
    for i in range(len(list_of_mols)):
        if rev:
            result[i, :list_of_mols[i].shape[0]] = list_of_mols[i][::-1]
        else:
            result[i, :list_of_mols[i].shape[0]] = list_of_mols[i]
    return fftpack.fft(result, n=trunct)


def all_vs_all(all_molecules, max_molecule_len):
    all_molecules = list_to_array(all_molecules, max_molecule_len, rev=False)
    pairwise_matrix = np.zeros((all_molecules.shape[0]**2))
    for i in range(all_molecules.shape[0]):
        shift = i * all_molecules.shape[0]
        one_to_all = one_vs_all(all_molecules[i], all_molecules)
        rev_to_all = one_vs_all(all_molecules[i][::-1], all_molecules)
        one_to_all = np.where(one_to_all > rev_to_all, np.abs(one_to_all - rev_to_all) + one_to_all,
                              np.abs(one_to_all - rev_to_all) + rev_to_all)
        max_corr = np.max(one_to_all)
        one_to_all *= 100
        one_to_all /= max_corr
        pairwise_matrix[shift: shift + all_molecules.shape[0]] = one_to_all/np.max(one_to_all)
    return pairwise_matrix

def normalize_and_obtain_centroids(unnormalized_matrix, centroid_number=100):
    whitened = whiten(unnormalized_matrix)
    c, l = kmeans2(whitened, centroid_number)
    return whitened, c
   
@jit
def all_vs_all_distances_v2(normalized_matrix: np.ndarray, centroids):    
    centroid_similarities = np.zeros((normalized_matrix.shape[0], centroids.shape[0]), dtype=float)
    for i in range(centroid_similarities.shape[0]):
        for ii in range(centroid_similarities.shape[1]):
            centroid_similarities[i,ii] = np.mean(np.abs(normalized_matrix[i] - centroids[ii]))
    number_of_molecules = centroid_similarities.shape[0]
    all_ssqs = np.zeros(number_of_molecules, dtype=np.float64)
    for i in range(number_of_molecules):
        all_ssqs[i] = get_ssq(centroid_similarities[i], centroids.shape[0])
    distance_matrix = np.zeros((number_of_molecules, number_of_molecules))
    for i in range(number_of_molecules):
        for ii in range(0, number_of_molecules, 1):
            distance_matrix[i, ii] = sum_of_sqrs(centroid_similarities[i], centroid_similarities[ii], all_ssqs[i], all_ssqs[ii], centroids.shape[0])
    return distance_matrix

@jit
def all_vs_all_distances(pairwise_matrix: np.ndarray, number_of_molecules, limit=100):
    entropies = np.argsort(diff_entropy(pairwise_matrix))[::-1][:limit]
    filtered_matrix = pairwise_matrix[:,entropies]
    all_ssqs = np.zeros(number_of_molecules, dtype=np.float64)
    for i in range(number_of_molecules):
        all_ssqs[i] = get_ssq(filtered_matrix[i], limit)
    distance_matrix = np.zeros((number_of_molecules, number_of_molecules))
    for i in range(number_of_molecules):
        for ii in range(0, number_of_molecules, 1):
            distance_matrix[i, ii] = sum_of_sqrs(filtered_matrix[i], filtered_matrix[ii], all_ssqs[i], all_ssqs[ii], limit)
    return distance_matrix

@jit
def all_vs_all_distances_filtered(filtered_matrix: np.ndarray, number_of_molecules, limit=100):
    all_ssqs = np.zeros(number_of_molecules, dtype=np.float64)
    for i in range(number_of_molecules):
        all_ssqs[i] = get_ssq(filtered_matrix[i], limit)
    distance_matrix = np.zeros((number_of_molecules, number_of_molecules))
    for i in range(number_of_molecules):
        for ii in range(0, number_of_molecules, 1):
            distance_matrix[i, ii] = sum_of_sqrs(filtered_matrix[i], filtered_matrix[ii], all_ssqs[i], all_ssqs[ii], limit)
    return distance_matrix

@jit
def get_ssq(mol, length):
    ssq = 0
    for i in range(length):
        ssq += mol[i]**2
    return ssq

@jit
def get_ssq_cross(mol1, mol2, length):
    ssq = 0
    for i in range(length):
        ssq += mol1[i]*mol2[i]
    return ssq
@jit
def sum_of_sqrs(current_mol, second_mol, current_mol_ssq, second_mol_ssq, length):
    cross = get_ssq_cross(current_mol, second_mol, length)
    cross_ssq = math.sqrt(current_mol_ssq*second_mol_ssq)
    return cross/cross_ssq


@jit
def get_bidirectional_edges(distance_matrix: np.ndarray, hits=10):
    result = np.zeros((distance_matrix.shape[0], hits))
    result[:,:] = -1
    sorted_matrix = sort_matrix(distance_matrix)
    for i in range(distance_matrix.shape[0]):
        ids = sorted_matrix[i][:hits]
        for r in range(len(ids)):
            if i in sorted_matrix[ids[r]][:hits]:
                result[i,r] = ids[r]
    return result


@jit
def normalized_correlation(original_molecules, bidirectional_results: np.ndarray):
    results = list()
    for i in range(bidirectional_results.shape[0]):
        mol1 = ms.MoleculeNoBack(original_molecules[i], snr=3)
        logmol1 = ms.MoleculeNoBack(np.log1p(mol1.nick_signal), snr=3)
        for ii in range(bidirectional_results.shape[1]):
            if bidirectional_results[i,ii] != i and bidirectional_results[i,ii] != -1:
                mol2 = ms.MoleculeNoBack(original_molecules[int(bidirectional_results[i, ii])], snr=3)
                logmol2 = ms.MoleculeNoBack(np.log1p(mol2.nick_signal), snr=3)
                corr = cs.CorrelationStruct(mol1, mol2)
                corr.correlate_with_zoom((0.95, 1.05))
                logcorr = cs.CorrelationStruct(logmol1, logmol2)
                logcorr.correlate_with_zoom((0.95, 1.05))
                results.append((logcorr.max_score, corr.max_score, logcorr, corr))
            else:
                pass
    return results


@jit
def sort_matrix(matrix):
    result = np.zeros(matrix.shape)
    for i in range(matrix.shape[0]):
        result[i] = np.argsort(matrix[i])[::-1]
    return result.astype(int)





