import networkx as nx
from OptiAsm import correlation_struct as cs
from OptiAsm import molecule_struct as ms
from OptiAsm import *

SNR = 4


class StringGraph:
    def __init__(self, molecule_corr_info_stream, dot_pics_path="./"):
        self.stream = molecule_corr_info_stream
        self.dot_pics_path = dot_pics_path
        self.edges_for_string_graph = molecule_corr_info_stream
        self.contained_edges = list()
        self.graph = nx.DiGraph()
        self.contigs = dict()
        self.graph2 = nx.DiGraph()
        self.reverse_table = dict()
        
    def _filter_overlaps_by_score_peaks(self):
        pass                      

    def create_graph(self):
        contained_nodes = set()
        #for edge in self.edges_for_string_graph:
        #    if edge["contained"]:
        #        self.contained_edges.append(edge)
        #        contained_nodes.add(edge["short_id"])
        for edge in self.edges_for_string_graph:
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes: 
                info_dict = dict()
                _long = edge["long_id"]
                _short = edge["short_id"]
                if edge["contained"] == False:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s .B" % _short, "%s .B" % _long, overhang=abs(edge["long_start"] - 0),
                                                label=[True, _long, edge["long_start"], 0])
                            self.graph.add_edge("%s .E" % _long, "%s .E" % _short, overhang=abs(edge["short_end"] -
                                                                                              edge["short_len"]),
                                                label=[True, _short, edge["short_end"], edge["short_len"]])
                        else:
                            self.graph.add_edge("%s .E" % _short, "%s .B" % _long, overhang=abs(edge["long_start"] - 0),
                                                label=[True,_long, edge["long_start"], 0])
                            self.graph.add_edge("%s .E" % _long, "%s .B" % _short, overhang=abs(edge["short_end"] - 0),
                                                label=[True, _short, edge["short_end"], 0])
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s .B" % _long, "%s .B" % _short, overhang=abs(edge["short_start"] - 0),
                                                label=[True, _short, edge["short_start"], 0])
                            self.graph.add_edge("%s .E" % _short, "%s .E" % _long, overhang=abs(edge["long_end"] -
                                                                                              edge["long_len"]),
                                                label=[True, _long, edge["long_end"], edge["long_len"]])
                        else:
                            self.graph.add_edge("%s .B" % _long, "%s .E" % _short, overhang=abs(edge["short_len"] -
                                                                                              edge["short_start"]),
                                                label=[True, _short, edge["short_len"], edge["short_start"]])
                            self.graph.add_edge("%s .B" % _short, "%s .E" % _long, overhang=abs(edge["long_end"] -
                                                                                              edge["long_len"]),
                                                label=[True, _long, edge["long_end"], edge["long_len"]])

    def create_graph_v2(self):
        contained_nodes = set()
        for edge in self.edges_for_string_graph:
            if edge["contained"]:
                self.contained_edges.append(edge)
#                contained_nodes.add(edge["short_id"])
        for edge in self.edges_for_string_graph:
            _long = edge["long_id"]
            _short = edge["short_id"]
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes: 
                if not edge["contained"]:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s.+" % _long, "%s.+" % _short)
                            self.graph.add_edge("%s.-" % _short, "%s.-" % _long)
                        else:
                            self.graph.add_edge("%s.+" % _long, "%s.-" % _short)
                            self.graph.add_edge("%s.+" % _short, "%s.-" % _long)
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph.add_edge("%s.+" % _short, "%s.+" % _long)
                            self.graph.add_edge("%s.-" % _long, "%s.-" % _short)
                        else:
                            self.graph.add_edge("%s.-" % _short, "%s.+" % _long)
                            self.graph.add_edge("%s.-" % _long, "%s.+" % _short)

    def create_graph_v4(self):
        contained_nodes = set()
        for edge in self.edges_for_string_graph:
            if edge["contained"]:
                self.contained_edges.append(edge)
                #contained_nodes.add(edge["long_id"])
        for edge in self.edges_for_string_graph:
            _long = edge["long_id"]
            _short = edge["short_id"]
            if not edge["short_id"] in contained_nodes and not edge["long_id"] in contained_nodes: 
                if not edge["contained"]:
                    if edge["long_start"] > 0:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph2.add_edge(_long, _short, type=(True, True),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])))
                            self.graph2.add_edge(_short, _long, type=(False, False),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])))
                        else:
                            self.graph2.add_edge(_long, _short, type=(True, False),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])))
                            self.graph2.add_edge(_short, _long, type=(True, False),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])))
                    else:
                        if edge["short_start"] < edge["short_end"]:
                            self.graph2.add_edge(_short, _long, type=(True, True),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])))
                            self.graph2.add_edge(_long, _short, type=(False, False),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])))
                        else:
                            self.graph2.add_edge(_short, _long, type=(False, True),
                                                 overlap=((edge["short_start"], edge["short_end"]),
                                                          (edge["long_start"], edge["long_end"])))
                            self.graph2.add_edge(_long, _short, type=(False, True),
                                                 overlap=((edge["long_start"], edge["long_end"]),
                                                          (edge["short_start"], edge["short_end"])))
                            
    def create_bfs_tree(self, sources, bfs_tree, graph):
        new_sources = set()
        if len(sources) == 0:
            return bfs_tree
        for node, direction in sources:
            for neighbor, edge in graph[node].items():
                if edge["type"][0] == direction:
                    if neighbor not in bfs_tree.nodes():
                        new_sources.add((neighbor, edge["type"][1]))
                        bfs_tree.add_edge(node, neighbor)
                else:
                    continue
        return self.create_bfs_tree(new_sources, bfs_tree, graph)
        
    def paths_from_subgraph(self, nodes, rem_bet=False, lim=3):
        subgraph = self.graph2.subgraph(nodes)
        if rem_bet:
            bet = nx.betweenness_centrality(subgraph)
            sorted_bet = sorted([(x,y) for y,x in bet.items()])[::-1]
            _1perc = int(0.005 * len(sorted_bet))
            _ = [subgraph.remove_node(x[1]) for x in sorted_bet[:_1perc]]
            new_graph = nx.Graph(subgraph)
            max_nodes = max([(len(x),x) for x in nx.connected_components(new_graph)])[1]
            subgraph = subgraph.subgraph(nodes)            
        cent = nx.degree_centrality(subgraph)
        sorted_cent = sorted([(x,y) for y,x in cent.items()])[::-1]
        i = np.argsort([x[0] for x in sorted_cent])[len(sorted_cent)//2]
        start_node = sorted_cent[i][1]
        tree = nx.DiGraph()
        t = self.create_bfs_tree([(start_node, True)], tree, subgraph)
        try:
            last_path = nx.dag_longest_path(tree)
        except ValueError:
            t = self.create_bfs_tree([(start_node, False)], tree, subgraph)
            last_path = nx.dag_longest_path(tree)
        plen = len(last_path)
        last_plen = 0 # int(plen) + 1
        while lim != 0:
            current_path = last_path
            if last_plen >= plen:
                lim -= 1
            #print("current path length:", last_plen)
            tree = nx.DiGraph()
            plen = last_plen
            t2 = self.create_bfs_tree([(last_path[-1], True)], tree, subgraph)
            try:
                last_path = nx.dag_longest_path(tree)
            except ValueError:
                t2 = self.create_bfs_tree([(last_path[-1], False)], tree, subgraph)
                last_path = nx.dag_longest_path(tree)
            last_plen = len(last_path)
        return current_path
    
    
    def obtain_components(self):
        temp_graph = nx.Graph(self.graph2)
        return list(nx.connected_components(temp_graph))
    
    
    
    def create_graph_v3(self):
        for edge in self.edges_for_string_graph:
            _long = edge["long_id"]
            _short = edge["short_id"]
            if not edge["contained"]:
                if edge["long_start"] > 0:
                    if edge["short_start"] < edge["short_end"]:
                        self.graph.add_edge("%s.+" % _long, "%s.+" % _short, contained=False, short=_short)
                        self.graph.add_edge("%s.-" % _short, "%s.-" % _long, contained=False, short=_short)
                    else:
                        self.graph.add_edge("%s.+" % _long, "%s.-" % _short, contained=False, short=_short)
                        self.graph.add_edge("%s.+" % _short, "%s.-" % _long, contained=False, short=_short)
                else:
                    if edge["short_start"] < edge["short_end"]:
                        self.graph.add_edge("%s.+" % _short, "%s.+" % _long, contained=False, short=_short)
                        self.graph.add_edge("%s.-" % _long, "%s.-" % _short, contained=False, short=_short)
                    else:
                        self.graph.add_edge("%s.-" % _short, "%s.+" % _long, contained=False, short=_short)
                        self.graph.add_edge("%s.-" % _long, "%s.+" % _short, contained=False, short=_short)
            else:
                    if edge["short_start"] < edge["short_end"]:
                        self.graph.add_edge("%s.+" % _long, "%s.+" % _short, contained=True, short=_short)
                        self.graph.add_edge("%s.-" % _long, "%s.-" % _short, contained=True, short=_short)
                    else:
                        self.graph.add_edge("%s.+" % _long, "%s.-" % _short, contained=True, short=_short)
                        self.graph.add_edge("%s.+" % _long, "%s.-" % _short, contained=True, short=_short)
    

    def mark_reducable_edges(self):
        for node in self.graph.nodes():
            edges = [desc for desc in nx.dfs_edges(self.graph, node, depth_limit=1)]
            sorted_nodes = sorted([(self.graph.edges[x]["overhang"], x) for x in edges], reverse=True)
            for overlap_length, node_id in sorted_nodes:
                if self.graph.edges[node_id]["label"][0]:
                    nested_edges = list(nx.dfs_edges(self.graph, node_id[0], depth_limit=1))
                    for _, nested_node in nested_edges:
                        if nested_node in nx.dfs_edges(self.graph, node, depth_limit=1):
                            self.graph.edges[node][nested_node]["label"][0] = False

    def mark_spurious_edge(self):
        pass

    def show_graph(self, dot_pic_name='temp_dot.png'):
        import matplotlib.pyplot as plt
        from networkx.drawing.nx_agraph import write_dot
        from subprocess import check_call as ck
        write_dot(self.graph, "unfiltered_graph.dot")
        ck("dot -Tpng unfiltered_graph.dot -o %s%s" % (self.dot_pics_path, dot_pic_name), shell=True)
        # plt.imshow(plt.imread(self.dot_pics_path + dot_pic_name))
        # plt.show();plt.cla()


def fish_out_corr(id_pair: (int,int), correlation_stream):
    for corr, id_pair_ in correlation_stream:
        if id_pair[0] in id_pair_ and id_pair[1] in id_pair_:
            return corr, id_pair
    return None


def assemble_pair(corr: cs.CorrelationStruct, plot=False):
    if corr.short_overlap[0] == 0:
        if plot:
            assembly = np.concatenate((corr.new_long[:int(corr.long_overlap[0])], corr.new_short[:]))
            short = np.zeros(assembly.shape)
            long = np.array(short)
            long[:corr.new_long.shape[0]] = corr.new_long[:]
            short[corr.long_overlap[0]:] = corr.new_short[:]
            return assembly, long, short, corr
        return np.concatenate((corr.new_long[:int(min(corr.long_overlap))], corr.new_short[:]))
    else:
        if plot:
            assembly = np.concatenate((corr.new_short[:int(corr.short_overlap[0])], corr.new_long[:]))
            short = np.zeros(assembly.shape)
            long = np.array(short)
            short[:corr.new_short.shape[0]] = corr.new_short[:]
            long[corr.short_overlap[0]:] = corr.new_long[:]
            return assembly, long, short, corr
        return np.concatenate((corr.new_short[:int(min(corr.short_overlap))], corr.new_long[:]))


def make_corr_pairs_from_molecules(mol1: np.ndarray, mol2: np.ndarray):
    mol1 = ms.MoleculeNoBack(mol1, SNR)
    mol2 = ms.MoleculeNoBack(mol2, SNR)
    corr = cs.CorrelationStruct(mol1, mol2)
    if corr.fit_for_cross:
        corr.correlate_with_zoom((0.98, 1.02))
        return corr
    else:
        raise ValueError


def find_and_assemble_pair(pair: (int, int), corrs: [cs.CorrelationStruct, (int, int)]):
    corr, real_pair = fish_out_corr(pair, corrs)
    return assemble_pair(corr)


def assembly_initiator(path: [int], corrs):
    new_path = list()
    for i in range(0, len(path) - 1, 1):
        id_pair = (path[i], path[i+1])
        new_path.append(find_and_assemble_pair(id_pair, corrs))
    return new_path


def recursively_assemble_from_path(path: [np.ndarray]):
    if len(path) == 1:
        return path[0]
    else:
        new_path = list()
        for i in range(0, len(path)-1, 1):
            new_path.append(assemble_pair(make_corr_pairs_from_molecules(path[i], path[i+1])))
    return recursively_assemble_from_path(new_path)


def assemble_from_path(path: [np.ndarray]):
    if len(path) == 1:
        return path[0]
    else:
        head_assembly = assemble_pair(make_corr_pairs_from_molecules(path[0], path[1]))
        return assemble_from_path([head_assembly] + path[2:])


def generate_graph_edges(correlation_stream):
    for correlation, id_pair in correlation_stream:
        yield correlation_to_edge(correlation, id_pair)


def correlation_to_edge(correlation: cs.CorrelationStruct, id_pair: tuple) -> dict:
    edge = dict()
    if correlation.long_id:
        edge["long_id"] = id_pair[0]
        edge["short_id"] = id_pair[1]
    else:
        edge["long_id"] = id_pair[1]
        edge["short_id"] = id_pair[0]
    edge["long_start"] = correlation.long_overlap[0]
    edge["long_end"] = correlation.long_overlap[1]
    if not correlation.reversed:
        edge["short_start"] = correlation.short_overlap[0]
        edge["short_end"] = correlation.short_overlap[1]
    else:
        edge["short_start"] = int(correlation.new_short.shape[0] - correlation.short_overlap[0])
        edge["short_end"] = int(correlation.new_short.shape[0] - correlation.short_overlap[1])
    edge["long_len"] = correlation.new_long.shape[0]
    edge["short_len"] = correlation.new_short.shape[0]
    if edge["short_len"] == abs(edge["short_end"] - edge["short_start"]):
        edge["contained"] = True
    else:
        edge["contained"] = False
    edge["reversed"] = correlation.reversed
    if edge["long_id"] != edge["short_id"]:
        return edge

