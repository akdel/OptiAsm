from subprocess import check_call as ck
import networkx as nx
from OptiAsm import correlation_struct as cs
from OptiAsm import molecule_struct as ms
from OptiAsm import bnx_utils as bnx
from OptiAsm import *
from OptiAsm import string_graph as sg


class MQR:
    def __init__(self, output_folder, ref_align_path):
        self.ref_align = ref_align_path
        self.output_dir = output_folder
        self.command = """%s -f -ref %s -i %s -o %s -nosplit 2 -BestRef 1 -biaswt 0 -Mfast 0 -FP 1.5 -FN 0.05 -sf 0.2 -sd 0.0 -A 5 -outlier 1e-3 -outlierMax 40 -endoutlier 1e-4 -S -1000 -sr 0.03 -se 0.2 -MaxSF 0.25 -MaxSE 0.5 -resbias 4 64 -maxmem 64 -M 3 3 -minlen 50 -T 1e-11 -maxthreads 32 -hashgen 5 3 2.4 1.5 0.05 5.0 1 1 3 -hash -hashdelta 10 -hashoffset 1 -hashmaxmem 64 -insertThreads 4 -maptype 0 -PVres 2 -PVendoutlier -AlignRes 2.0 -rres 0.9 -resEstimate -ScanScaling 2 -RepeatMask 5 0.01 -RepeatRec 0.7 0.6 1.4 -maxEnd 50 -usecolor 1 -stdout -stderr -randomize 1 -subset 1 5000"""
        self.xmap = None

    def run_ref_align(self, reference_cmap_path, bnx_file):
        ck(self.command % (self.ref_align, reference_cmap_path, bnx_file, self.output_dir + "bnx_quality"), shell=True)

    def load_results(self):
        xmap_file = self.output_dir + 'bnx_quality.xmap'
        self.xmap = bnx.XmapParser(xmap_file)

    def corrs_to_paths(self, corrs, bet_thr=0.05, edge_bet=0.05):
        import networkx as nx
        edges = list([x for x in sg.generate_graph_edges(corrs) if x != None])
        gr = sg.StringGraph(edges)
        gr.create_graph_v4()
      #  cent = nx.degree_centrality(gr.graph2)
      #  sorted_cent = sorted([(x,y) for  y,x in cent.items()])[::-1]
      #  _01perc = int(len(sorted_cent)*0.01)
      #  _01lim = sorted_cent[_01perc][0]
      #  _ = [gr.graph2.remove_node(x) for x,y in cent.items() if y > _01lim]
      #  print(len(_))
      #  print(gr.graph2.number_of_nodes())
        comps = gr.obtain_components()
        paths = []
        undirected = nx.Graph(gr.graph2)
        for comp in comps:
            if len(comp) > 200:
                print(len(comp))
                
                subgraph = undirected.subgraph(comp)
                bet = nx.load_centrality(subgraph, cutoff=5)
                print("load found")
                sorted_bet = sorted([(x,y) for  y,x in bet.items()])[::-1]
                _01perc = int(len(sorted_bet)*bet_thr)
                _01lim = sorted_bet[_01perc][0]
                print("start chimera")
                _ = [(gr.graph2.remove_node(x), subgraph.remove_node(x),print(x)) for x,y in bet.items() if y > _01lim]
                print("end chimera")
                edge_dict = nx.edge_betweenness_centrality(subgraph, k=int(subgraph.number_of_nodes()/20))
                print("edge betweenness found")
                sorted_edge_dict = sorted([(x,y) for y,x in edge_dict.items()])[::-1]
                _01perc = int(len(sorted_edge_dict)*edge_bet)
                _01lim = sorted_edge_dict[_01perc][0]
                print(_01lim)
                _ = [(subgraph.remove_edge(x[0],x[1])) for x,y in edge_dict.items() if y > _01lim]
                print(len(_))
                
                comp2s = list(nx.connected_components(subgraph))
                for comp2 in comp2s:
                    if len(comp2) > 1:
                        paths.append(gr.paths_from_subgraph(comp2))
            else:
                if len(comp) > 1:
                    paths.append(gr.paths_from_subgraph(comp))
        return paths


    def corrs_to_paths2(self, corrs, bet_thr=0.05, edge_bet=0.05):
        import networkx as nx
        edges = list([x for x in sg.generate_graph_edges(corrs) if x != None])
        gr = sg.StringGraph(edges)
        gr.create_graph_v4()
      #  cent = nx.degree_centrality(gr.graph2)
      #  sorted_cent = sorted([(x,y) for  y,x in cent.items()])[::-1]
      #  _01perc = int(len(sorted_cent)*0.01)
      #  _01lim = sorted_cent[_01perc][0]
      #  _ = [gr.graph2.remove_node(x) for x,y in cent.items() if y > _01lim]
      #  print(len(_))
      #  print(gr.graph2.number_of_nodes())
        comps = gr.obtain_components()
        paths = []
        undirected = nx.Graph(gr.graph2)
        for comp in comps:
            if len(comp) > 200:
                print(len(comp))
                
                subgraph = undirected.subgraph(comp)
                bet = nx.load_centrality(subgraph, cutoff=5)
                print("load found")
                sorted_bet = sorted([(x,y) for  y,x in bet.items()])[::-1]
                _01perc = int(len(sorted_bet)*bet_thr)
                _01lim = sorted_bet[_01perc][0]
                print("start chimera")
                _ = [(gr.graph2.remove_node(x), subgraph.remove_node(x),print(x)) for x,y in bet.items() if y > _01lim]
                print("end chimera")
                edge_dict = nx.edge_betweenness_centrality(subgraph, k=int(subgraph.number_of_nodes()/20))
                print("edge betweenness found")
                sorted_edge_dict = sorted([(x,y) for y,x in edge_dict.items()])[::-1]
                _01perc = int(len(sorted_edge_dict)*edge_bet)
                _01lim = sorted_edge_dict[_01perc][0]
                print(_01lim)
                _ = [(subgraph.remove_edge(x[0],x[1])) for x,y in edge_dict.items() if y > _01lim]
                print(len(_))
                
                comp2s = list(nx.connected_components(subgraph))
                for comp2 in comp2s:
                    if len(comp2) > 1:
                        p = gr.paths_from_subgraph(comp2)
                        paths.append((p, gr.graph2.subgraph(p)))
            else:
                if len(comp) > 1:
                    p = gr.paths_from_subgraph(comp)
                    paths.append((p, gr.graph2.subgraph(p)))
        return paths
    
    def paths_to_bnx(self, paths, mols, output):
        from Photo_map import database as db
        i = 0
        for path in paths:
            i += 1
            f_name = output + '%s_bnx.bnx'
            db.molecules_to_bnx([(mols[x], mols[x]) for x in path], 10, 500,
                                f_name % i, bnx_template_path="/mnt/nexenta/akdel001/phd/Code/photomap2/photomap/bnx_head.txt"
                                )

    def bnx_folder_to_xmaps(self,reference_cmap, bnx_folder):
        from os import listdir
        xmaps = []
        bnxpaths = [bnx_folder + x for x in listdir(bnx_folder)]
        for b in bnxpaths:
            self.run_ref_align(reference_cmap, b)
            self.load_results()
            xmap = self.xmap
            xmap.read_and_load_cmap_file()
            xmap.get_intervals()
            xmaps.append(xmap)
        return xmaps
