import numpy as np
from OptiAsm import pairwise_cross_corr as pw
from sys import argv

if __name__ == "__main__":
    mol_file = argv[1]
    thread_num = int(argv[2])
    mols = np.load(mol_file)[:50000]
    log_file = open("corr_log.txt", "w")
    log_file.write("mols loaded \n")
    log_file.flush()
    log_mols = pw.to_log_mols(mols)
    log_file.write("log mols loaded \n")
    log_file.flush()
    fft_mols = pw.CrossCorrInput(mols, 1, trunct=128, to_log=True, spectra_ratio=0.1)
    log_file.write("fft mols loaded \n")
    log_file.flush()
    fft_mols.split_to_thread_jobs_in_sequence()
    m = pw.CrossCorrMemmap(fft_mols, "/home/akdel001/cross_corr.mem")
    log_file.write("memmap started\n")
    log_file.flush()
    pw.obtain_assembly_corrs(mols, log_mols, m, t=thread_num)